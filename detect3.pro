TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt

# Settings for external libraries

## Boost, Boost.Asynchronous
isEmpty(BOOST_ASYNCHRONOUS_INCLUDE_PATH) { BOOST_ASYNCHRONOUS_INCLUDE_PATH = /usr/local/include }

INCLUDEPATH += $${BOOST_ASYNCHRONOUS_INCLUDE_PATH}
DEFINES += BOOST_ASYNCHRONOUS_REQUIRE_ALL_ARGUMENTS BOOST_ASYNCHRONOUS_PRCTL_SUPPORT
LIBS    += -lboost_thread -lboost_system -lpthread

## libfreenect2
LIBS += -lfreenect2 -lOpenCL

## OpenCV
LIBS += -lopencv_highgui -lopencv_core

## Tensorflow
isEmpty(TENSORFLOW_LIB_PATH)     { TENSORFLOW_LIB_PATH     = /usr/local/lib }
isEmpty(TENSORFLOW_INCLUDE_PATH) { TENSORFLOW_INCLUDE_PATH = /usr/local/include/tf }
isEmpty(EIGEN3_INCLUDE_PATH)     { EIGEN3_INCLUDE_PATH     = /usr/include/eigen3 }
isEmpty(CUDA_INCLUDE_PATH)       { CUDA_INCLUDE_PATH       = /opt/cuda/include }

LIBS += -L$${TENSORFLOW_LIB_PATH} -ltensorflow_cc -lprotobuf
INCLUDEPATH += $${TENSORFLOW_INCLUDE_PATH} \
               $${EIGEN3_INCLUDE_PATH} \
               $${CUDA_INCLUDE_PATH}

## FLANN
LIBS += -lflann

# detect3

SOURCES += main.cpp

HEADERS += \
    concurrency.hpp \
    config.hpp \
    hardware/hardware.hpp \
    hardware/kinect2/acquisition.hpp \
    hardware/kinect2/framepack.hpp \
    hardware/kinect2/registration.hpp \
    log.hpp \
    pipeline/featureextraction.hpp \
    pipeline/matching.hpp \
    pipeline/sampling.hpp \
    pipeline/types.hpp \
    pipeline/votefiltering.hpp \
    visual/visualization.hpp \
    visual/traits.hpp
