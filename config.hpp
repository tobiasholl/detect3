#ifndef CONFIG_HPP
#define CONFIG_HPP

namespace Config
{

// Kinect 2 settings (hardware/kinect2/...)
constexpr int    KINECT2_REGISTRATION_STAGE_THREADS = 1;
constexpr size_t KINECT2_IMAGE_WIDTH                = 512;
constexpr size_t KINECT2_IMAGE_HEIGHT               = 424;

// Patch extraction settings (pipeline/patch.hpp) (see e.g. Kehl et al., p. 4)
constexpr int    PATCH_METRIC_SIZE = 50; // in mm
constexpr int    PATCH_PIXEL_SIZE  = 32; // in pixels

// Sampling settings (pipeline/sampling.hpp)
constexpr int    SAMPLING_STAGE_THREADS       = 2;
constexpr long   SAMPLING_PARALLEL_FOR_CUTOFF = 1024;
constexpr int    SAMPLING_DEFAULT_STEP        = 16; // TODO: try 8 again, even though that effectively halves framerate or worse

// Feature extraction settings (pipeline/featureextraction.hpp)
constexpr int    FEATURE_EXTRACTION_STAGE_THREADS = 1;
constexpr int    FEATURE_EXTRACTION_DIMENSIONS    = 128;
constexpr int    TENSORFLOW_THREADS               = 4;

// Matching settings (pipeline/matching.hpp)
constexpr int    MATCHING_STAGE_THREADS = 2;
constexpr int    MATCHING_NEIGHBORS     = 3; // Parameter 'k' in Kehl et al., cf. p. 18
constexpr int    FLANN_THREADS          = 4;

// Vote filtering (pipeline/votefiltering.hpp)
constexpr int    VOTE_FILTERING_STAGE_THREADS   = 2;
constexpr int    VOTE_FILTERING_CELL_SIZE       = 5;
constexpr int    VOTE_FILTERING_CANDIDATES      = 5; // Parameter 'N' in Kehl et al., cf. p. 10 et seq.
constexpr float  VOTE_FILTERING_CONVERGENCE     = 0.0001;
constexpr int    VOTE_FILTERING_LOCATION_KERNEL = 25; // in mm
constexpr int    VOTE_FILTERING_ROTATION_KERNEL = 7;  // in degrees

} // namespace Config

#endif // CONFIG_HPP
