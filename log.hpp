#ifndef LOG_HPP
#define LOG_HPP

#include <ctime>
#include <iomanip>
#include <iostream>
#include <memory>

#include "concurrency.hpp"

namespace Log
{

// LogLevel: Describes the level of importance of a logged message.

enum LogLevel
{
    Error   = 1,
    Warning = 2,
    Verbose = 3,
    Debug   = 4
};


// LogServant: Responsible for all things related to logging. This ensures that output to the terminal only happens from one thread. While std::cout is threadsafe, this avoids mangled output.

class LogServant
{
public:
    LogServant(LogLevel level = LogLevel::Warning, bool printTime = false, bool enableColor = true) : m_level(level), m_time(printTime), m_color(enableColor) {}

    template <typename... Args>
    void log(LogLevel level, Args&&... args)
    {
        if (level <= m_level)
            ((std::cout << colorStart(level) << currentTime() << "[" << levelString(level) << "]   ") << ... << args) << colorEnd() << std::endl;
    }

    void setLevel(LogLevel level)   { m_level = level; }
    void setTime(bool printTime)    { m_time  = printTime; }
    void setColor(bool enableColor) { m_color = enableColor; }

private:
    std::string levelString(LogLevel level) const
    {
        switch (level)
        {
            case LogLevel::Error:   return "  ERROR  ";
            case LogLevel::Warning: return " WARNING ";
            case LogLevel::Verbose: return " VERBOSE ";
            case LogLevel::Debug:   return "  DEBUG  ";
            default:                return " INVALID ";
        }
    }

    std::string currentTime() const
    {
        if (!m_time) return "";

        // Get UTC time
        timespec utc;
        timespec_get(&utc, TIME_UTC);

        // Convert UTC time to local time
        tm *local = localtime(&utc.tv_sec);

        // Convert time to string (ISO format ("%y-%m-%d %H:%M:%S"))
        char buf[TIME_BUFFER_SIZE];
        strftime(buf, TIME_BUFFER_SIZE, "%F %T", local);

        // Add milliseconds
        std::ostringstream formatter;
        formatter << buf << "." << std::setfill('0') << std::setw(3) << (utc.tv_nsec / 1000000ULL) << "   ";
        return formatter.str();
    }

    std::string colorStart(LogLevel level)
    {
        if (!m_color) return "";
        switch (level)
        {
            case LogLevel::Error:   return "\x1b[31;1m"; // Bold red
            case LogLevel::Warning: return "\x1b[33m";   // Normal yellow
            case LogLevel::Verbose: return "";           // Normal white
            case LogLevel::Debug:   return "\x1b[2m";    // Dim white
            default:                return "\x1b[36;1m"; // Bold cyan
        }
    }

    std::string colorEnd()
    {
        return m_color ? "\x1b[0m" : "";
    }

    LogLevel m_level;
    bool     m_time;
    bool     m_color;

    constexpr static int TIME_BUFFER_SIZE = 20; // 19 characters for ISO format + '\0'.
};

class LogProxy
    : public boost::asynchronous::servant_proxy<LogProxy, LogServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    LogProxy(Scheduler s, LogLevel level = LogLevel::Warning, bool time = false, bool color = true)
        : boost::asynchronous::servant_proxy<LogProxy, LogServant, Concurrent::Job>(s, level, time, color)
    {}

    BOOST_ASYNC_POST_MEMBER_LOG(log,      "LogProxy::log",      Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(setLevel, "LogProxy::setLevel", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(setTime,  "LogProxy::setTime",  Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(setColor, "LogProxy::setColor", Concurrent::NORMAL_PRIORITY)
};


// Global logging functions: There should only ever be one LogServant; use the Log::log and Log::setLevel functions instead of creating a new LogProxy.

#ifndef LOG_DEFAULT_LEVEL
    // Set the default log level to Warning in release builds and to Debug in debug builds
    #ifdef NDEBUG
        #define LOG_DEFAULT_LEVEL ::Log::LogLevel::Warning
    #else
        #define LOG_DEFAULT_LEVEL ::Log::LogLevel::Debug
    #endif
#endif
#ifndef LOG_SHOW_TIME
    // By default, disable timestamps
    #define LOG_SHOW_TIME false
#endif
#ifndef LOG_ENABLE_COLORS
    // By default, enable colors
    #define LOG_ENABLE_COLORS true
#endif

static auto globalLog()
{
    static auto globalLogProxy = std::make_shared<LogProxy>(Concurrent::createSingleThreadScheduler("LogScheduler"), LOG_DEFAULT_LEVEL, LOG_SHOW_TIME, LOG_ENABLE_COLORS);
    return globalLogProxy;
}

template <typename... Args>
void log(LogLevel level, Args&&... args)
{
    globalLog()->log(level, std::forward<Args>(args)...);
}

void setLevel(LogLevel level = LOG_DEFAULT_LEVEL)
{
    globalLog()->setLevel(level);
}

void setTime(bool printTime = LOG_SHOW_TIME)
{
    globalLog()->setTime(printTime);
}

void setColor(bool enableColors = LOG_ENABLE_COLORS)
{
    globalLog()->setColor(enableColors);
}

} // namespace Log

#endif // LOG_HPP
