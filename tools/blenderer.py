# This script should be run from inside Blender, after
# loading a 3D model of the target object

import os
import shutil

# The default rotations form an icosahedron.
# _tf and _tb are the tilt angles for the 18 inner vertices
_tf = pi / 2 - atan(0.5)
_tb = pi / 2 + atan(0.5)
f = lambda x: radians(x * 72)
b = lambda x: radians(36 + x * 72)
icosahedron = [(0,   0,    0),  # Front
               (0,   0,    pi), # Back
               (_tf, f(0), 0),  # Front ring
               (_tf, f(1), 0),
               (_tf, f(2), 0),
               (_tf, f(3), 0),
               (_tf, f(4), 0),
               (_tb, b(0), 0),  # Back ring
               (_tb, b(1), 0),
               (_tb, b(2), 0),
               (_tb, b(3), 0),
               (_tb, b(4), 0)]

# Configuration
ROTATIONS = icosahedron
WIDTH     = 512
HEIGHT    = 424

# Get the current directory
directory = os.getcwd()

# Add an Empty
bpy.ops.object.empty_add()

# Grab the camera (Camera) and the new object
camera = bpy.data.objects["Camera"]
camDat = bpy.data.cameras["Camera"]
origin = bpy.data.objects["Empty"]

# Make sure the Empty is at the origin
origin.location       = (0, 0, 0)
origin.rotation_euler = (0, 0, 0)
origin.animation_data_clear()

# Parent the camera to the Empty
camera.parent = origin

# Set up render graph to render depth maps
bpy.context.scene.use_nodes = True

# Write color images
# Set up render graph to render depth maps and masks in addition to normal images
tree  = bpy.context.scene.node_tree
links = tree.links

# Create new nodes
rlayers_node = tree.nodes["Render Layers"]
map_node     = tree.nodes.new("CompositorNodeMapRange")
mix_node     = tree.nodes.new("CompositorNodeMixRGB")
max_node     = tree.nodes.new("CompositorNodeMath")
output_node  = tree.nodes.new("CompositorNodeOutputFile")

# Configure nodes
map_node.use_clamp = True
map_node.inputs["From Min"].default_value = camDat.clip_start
map_node.inputs["From Max"].default_value = camDat.clip_end
mix_node.use_clamp = True
mix_node.inputs[1].default_value = [0.0, 0.0, 0.0, 1.0]
mix_node.inputs[2].default_value = [1.0, 1.0, 1.0, 1.0]
max_node.operation = "LESS_THAN"
max_node.inputs[1].default_value = camDat.clip_end
output_node.base_path = directory
output_node.layer_slots.new("Mask")
output_node.format.color_depth = "16"
output_node.format.color_mode = "BW"
output_node.file_slots[0].path = "depth_tmp_"
output_node.file_slots[1].path = "mask_tmp_"

# Create links
links.new(rlayers_node.outputs["Depth"], map_node.inputs["Value"])
links.new(rlayers_node.outputs["Depth"], max_node.inputs[0])
links.new(map_node.outputs["Value"], mix_node.inputs["Fac"])
links.new(mix_node.outputs["Image"], output_node.inputs["Image"])
links.new(max_node.outputs["Value"], output_node.inputs["Mask"])

# Render each rotation
for index, rotation in enumerate(ROTATIONS):
    origin.rotation_euler = rotation
    # Render color explicitly, this should also dump depth and mask images
    color_filename = "color_{}.png".format(index)
    bpy.data.scenes["Scene"].render.filepath = os.path.join(directory, color_filename)
    bpy.data.scenes['Scene'].render.resolution_x = WIDTH * 2
    bpy.data.scenes['Scene'].render.resolution_y = HEIGHT * 2
    bpy.ops.render.render(write_still=True)
    # Now, move the depth_ and mask_ files
    shutil.move(os.path.join(directory, "depth_tmp_0001.png"),
                os.path.join(directory, "depth_{}.png".format(index)))
    shutil.move(os.path.join(directory, "mask_tmp_0001.png"),
                os.path.join(directory, "mask_{}.png".format(index)))
    # Write the rotation data
    with open("rot_{}.txt".format(index), "w") as rot_file:
        rot_file.write("{} {} {}\n".format(*rotation))


