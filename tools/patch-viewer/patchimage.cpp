#include "patchimage.hpp"

#include <fstream>

PatchImage::PatchImage(QQuickItem *parent)
    : QQuickPaintedItem(parent)
    , m_colorImage(320, 320, QImage::Format_RGB888)
    , m_depthImage(320, 320, QImage::Format_RGB888)
{}

QUrl PatchImage::path() const
{
    return m_path;
}

void PatchImage::setPath(const QUrl& path)
{
    m_path = path;
    m_raw.clear();

    std::ifstream reader(m_path.toLocalFile().toStdString(), std::ios_base::binary);
    while (reader)
    {
        Patch patch;
        reader.read((char*) patch.raw, Patch::FLOATS * sizeof(float));
        m_raw.push_back(std::move(patch));
    }
}

int PatchImage::index() const
{
    return m_index;
}

void PatchImage::setIndex(int index)
{
    if (m_raw.size()) m_index = (index + m_raw.size()) % m_raw.size();
    else { m_index = 0; return; }

    Patch& patch = m_raw[m_index];
    auto color_at = [&patch](size_t row, size_t col)
    {
        unsigned char r = 255.f * (patch.r[row * Config::PATCH_PIXEL_SIZE + col] + 1.f);
        unsigned char g = 255.f * (patch.g[row * Config::PATCH_PIXEL_SIZE + col] + 1.f);
        unsigned char b = 255.f * (patch.b[row * Config::PATCH_PIXEL_SIZE + col] + 1.f);
        return QColor(r, g, b);
    };
    auto depth_at = [&patch](size_t row, size_t col)
    {
        unsigned char d = 255.f * (patch.d[row * Config::PATCH_PIXEL_SIZE + col] + 1.f);
        return QColor(d, d, d);
    };
    for (size_t row = 0; row < 320; ++row)
    {
        for (size_t col = 0; col < 320; ++col)
        {
            m_colorImage.setPixelColor(row, col, color_at(row / 10, col / 10));
            m_depthImage.setPixelColor(row, col, depth_at(row / 10, col / 10));
        }
    }

    onFileChanged(file());
    update();
}

QString PatchImage::file() const
{
    return m_path.toLocalFile();
}

void PatchImage::paint(QPainter* painter)
{
    if (m_index < 0 || m_index >= (ssize_t) m_raw.size()) return;
    painter->drawImage(0,   0, m_colorImage);
    painter->drawImage(320, 0, m_depthImage);
}
