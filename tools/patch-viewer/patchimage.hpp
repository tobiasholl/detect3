#ifndef PATCHIMAGE_HPP
#define PATCHIMAGE_HPP

#include <QtQuick/QQuickPaintedItem>
#include <QImage>
#include <QPainter>

#include <common.hpp>

class PatchImage : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QUrl    path  READ path  WRITE setPath)
    Q_PROPERTY(int     index READ index WRITE setIndex)
    Q_PROPERTY(QString file  READ file  NOTIFY onFileChanged)

public:
    PatchImage(QQuickItem *parent = nullptr);

    QUrl path() const;
    void setPath(QUrl const& path);

    int index() const;
    void setIndex(int index);

    QString file() const;

    void paint(QPainter *painter);

signals:
    void onFileChanged(QString newFile);

private:
    QUrl m_path;
    int  m_index;

    // Internal
    int m_images;
    std::vector<Patch> m_raw;
    QImage m_colorImage;
    QImage m_depthImage;
};

#endif // PATCHIMAGE_HPP
