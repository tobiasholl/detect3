QT += quick
CONFIG += c++1z
DEFINES += QT_DEPRECATED_WARNINGS

# Tools
include(../common/common.pri)

# patch-viewer
SOURCES += \
    patchimage.cpp \
    patch-viewer.cpp
HEADERS += \
    patchimage.hpp
RESOURCES += qml.qrc
