import QtQuick 2.8
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import PatchViewer 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 400
    title: "Patch Viewer"

    menuBar: MenuBar {
        Menu {
            title: "File"
            MenuItem {
                text: "Open..."
                onTriggered: {
                    openDialog.visible = true;
                }
            }
            MenuItem {
                text: "Quit"
                onTriggered: {
                    Qt.quit();
                }
            }
        }
        Menu {
            title: "Image"
            MenuItem {
                text: "Previous"
                onTriggered: {
                    patch.index -= 1;
                }
            }
            MenuItem {
                text: "Next"
                onTriggered: {
                    patch.index += 1;
                }
            }
        }
    }

    Item {
        anchors.fill: parent

        FileDialog {
            id: openDialog
            selectExisting: true
            selectFolder:   false
            selectMultiple: false
            title: "Open patch set"

            onAccepted: {
                visible = false;
                patch.path = openDialog.fileUrl;
                patch.index = 0;
            }
        }

        PatchImage {
            id: patch
            index: 0
            height: 320
            width: 640
            anchors.top: parent.top
            anchors.left: parent.left
        }

        Text {
            id: patchLabel
            anchors.top: patch.bottom
            anchors.horizontalCenter: patch.horizontalCenter
            anchors.margins: 5
            font.pointSize: 14
            text: patch.file
        }


        focus: true
        Keys.onUpPressed: patch.index -= 1
        Keys.onDownPressed: patch.index += 1
        Keys.onLeftPressed: patch.index -= 1
        Keys.onRightPressed: patch.index += 1
    }
}
