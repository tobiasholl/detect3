#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "patchimage.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<PatchImage>("PatchViewer", 1, 0, "PatchImage");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
