TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt

# Tools
include(../common/common.pri)

# External libraries
LIBS += -lpng # png++

# patch-gen
SOURCES += \
    patch-gen.cpp
