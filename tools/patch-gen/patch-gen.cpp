#include <cmath>
#include <iostream>
#include <tuple>
#include <memory>
#include <algorithm>
#include <fstream>

#include <png++/png.hpp>

#include <common.hpp>

// You should try to keep this up-to-date with detect3's sampling.hpp

constexpr double M_SQRT3_2 = sqrt(3) / 2;

struct PYR { float pitch, roll, yaw; };

void eat(float fx, float fy, Image const& color, Image const& depthImage, Image const& mask, PYR const& rotation, int step, std::string const& outfile, std::string const& token = "")
{
    // Get image dimensions
    size_t width  = color.width;
    size_t height = color.height;

    // Get line step
    float lineStep = M_SQRT3_2 * step;

    // Get depth at the image center
    float centerDepth = *(depthImage.data.get() + (height / 2) * width + width / 2);

    // Get the margins and step counts (see description above)
    //     VERTICAL SAMPLES   = (height - 1) / d + 1
    //     HORIZONTAL SAMPLES = (width - 1 - s / 2) / s + 1
    //     VERTICAL MARGIN    = (height - (d * (V. SAMPLES - 1) + 1)) / 2
    //     HORIZONTAL MARGIN  = (width - (s * (H. SAMPLES - 1) + 1)) / 2
    size_t verticalSamples   = (size_t) ((height - 1) / lineStep + 1);
    size_t horizontalSamples = (size_t) ((width - 1 - step / 2.f) / step + 1);
    size_t verticalMargin   = (size_t) ((height - (lineStep * (verticalSamples - 1) + 1)) / 2.f);
    size_t horizontalMargin = (size_t) ((width - (step * (horizontalSamples - 1) + 1)) / 2); // No 2.f here because the entire calculation is in integers

    size_t totalSamples = verticalSamples * horizontalSamples;

    auto sampleToImage = [horizontalSamples, horizontalMargin, verticalMargin, step, lineStep](size_t const& index) -> std::tuple<size_t, size_t>
    {
        // Translate the index into a pixel coordinate on the image.
        size_t sampleRow = index / horizontalSamples;
        size_t imageRow  = verticalMargin + sampleRow * lineStep;
        size_t sampleCol = index % horizontalSamples;
        size_t imageCol  = horizontalMargin + (sampleRow % 2 ? step / 2 : 0) + sampleCol * step;

        return {imageRow, imageCol};
    };

    // Sample the image
    for (size_t index = 0; index < totalSamples; ++index)
    {
        auto coords = sampleToImage(index);
        // Get sampling coordinates
        //auto [row, col] = coords;
        auto row = std::get<0>(coords);
        auto col = std::get<1>(coords);

        // Get depth in millimeters
        float *rawDepth = depthImage.data.get();
        float depth = *(rawDepth + row * width + col);

        // Skip invalid samples
        if (depth <= 0 || std::isnan(depth) || std::isinf(depth))
        {
            std::cerr << "Skipping patch " << index << " at token " << token << ": Invalid depth" << std::endl;
            continue;
        }

        // Calculate extraction patch size in pixels
        size_t extractionWidth  = Config::PATCH_METRIC_SIZE / depth * fx;
        size_t extractionHeight = Config::PATCH_METRIC_SIZE / depth * fy;

        // Skip patches that cannot be extracted in full
        ssize_t topLeftRow = row - extractionHeight / 2;
        ssize_t topLeftCol = col - extractionWidth / 2;
        if (topLeftRow < 0 || topLeftRow + extractionHeight >= height || topLeftCol < 0 || topLeftCol + extractionWidth >= width)
        {
            std::cerr << "Skipping patch " << index << " at token " << token << ": Margin exceeded" << std::endl;
            continue;
        }

        // Check for mask overlap
        bool anyMask = false;
        for (size_t extractRow = 0; extractRow < extractionHeight; ++extractRow)
        {
            for (size_t extractCol = 0; extractCol < extractionWidth; ++extractCol)
            {
                if (*(mask.data.get() + (topLeftRow + extractRow) * width + (topLeftCol + extractCol)))
                {
                    anyMask = true;
                    break;
                }
            }
            if (anyMask) break;
        }
        if (!anyMask)
        {
            std::cerr << "Skipping patch " << index << " at token " << token << ": No overlap with object" << std::endl;
            continue;
        }

        // Compute scale necessary to reach predefined patch size
        float scaleX = (float) extractionWidth / Config::PATCH_PIXEL_SIZE;
        float scaleY = (float) extractionHeight / Config::PATCH_PIXEL_SIZE;

        // Extract the patch
        Patch patch;
        patch.dx    = (col - width / 2.f) * depth / fx;
        patch.dy    = (row - height / 2.f) * depth / fy;
        patch.dz    = depth - centerDepth;
        patch.pitch = rotation.pitch;
        patch.yaw   = rotation.yaw;
        patch.roll  = rotation.roll;

        // Get raw pointers
        unsigned char *rawColor = (unsigned char*) color.data.get();

        for (size_t patchRow = 0; patchRow < Config::PATCH_PIXEL_SIZE; ++patchRow)
        {
            for (size_t patchCol = 0; patchCol < Config::PATCH_PIXEL_SIZE; ++patchCol)
            {
                // Compute offset in the unscaled patch
                size_t offsetRow = (size_t) ((patchRow + .5f) * scaleY);
                size_t offsetCol = (size_t) ((patchCol + .5f) * scaleX);

                // Compute original image pixel
                size_t originalRow = topLeftRow + offsetRow;
                size_t originalCol = topLeftCol + offsetCol;

                // Query mask
                bool isOutsideOfObject = *(mask.data.get() + originalRow * width + originalCol) == 0;

                // Scale colors from [0; 255] to [-1; 1]
                unsigned char *colorPointer = rawColor + (originalRow * width + originalCol) * sizeof(float);
                patch.b[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = isOutsideOfObject ? -1.f : (colorPointer[0] / 127.5f - 1.f);
                patch.g[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = isOutsideOfObject ? -1.f : (colorPointer[1] / 127.5f - 1.f);
                patch.r[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = isOutsideOfObject ? -1.f : (colorPointer[2] / 127.5f - 1.f);

                // De-mean and scale depth (from [z-m; z+m] via [-m; m] to [-1; 1]) and clamp
                patch.d[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = isOutsideOfObject ? 1.f : (std::clamp((rawDepth[originalRow * width + originalCol] - depth) / Config::PATCH_METRIC_SIZE, -1.f, 1.f));
            }
        }

        // Write patches
        std::ofstream writer(outfile, std::ios_base::binary | std::ios_base::app);
        writer.write((char*) patch.raw, Patch::FLOATS * sizeof(float));
    }
}


int main(int argc, char *argv[])
{
    // patch-gen is constructed to fit the format of the data generated by blenderer.py
    // Data format:
    //    - color_?.png: 8-bit RGBA PNG image (RGB data)
    //    - depth_?.png: 16-bit Grayscale PNG image (depth data, black is close, white is distant)
    //    - mask_?.png:  16-bit Grayscale PNG image (mask data, black is background, white is foreground)
    //    - pose_?.txt:  Rotation data. We assume the center of the image is at x = y = 0. Contains three numbers (Blender's "XYZ Euler" is a (pitch, roll, yaw) tuple in radians).
    // Adapt or convert as needed.

    if (argc != 5) throw std::logic_error("Usage: patch-gen FX FY DATASET OUTPUT-DIRECTORY");
    int arg = 1;

    float       fx      = std::atof(argv[arg++]);
    float       fy      = std::atof(argv[arg++]);
    std::string dataset = argv[arg++];
    std::string outfile = argv[arg++];

    try
    {
        for (int i = 0;; ++i)
        {
            std::string index = std::to_string(i);
            std::string colorPath = dataset + "/color_" + index + ".png";
            std::string depthPath = dataset + "/depth_" + index + ".png";
            std::string maskPath  = dataset + "/mask_"  + index + ".png";
            std::string rotPath   = dataset + "/rot_"   + index + ".png";

            png::image<png::rgba_pixel> colorPng(colorPath, png::require_color_space<png::rgba_pixel>());
            png::image<png::gray_pixel_16> depthPng(depthPath, png::require_color_space<png::gray_pixel_16>());
            png::image<png::gray_pixel_16> maskPng(maskPath, png::require_color_space<png::gray_pixel_16>());
            std::ifstream rotStream(rotPath);

            uint32_t width  = colorPng.get_width();
            uint32_t height = colorPng.get_height();
            uint32_t area   = width * height;
            Image color { width, height, std::shared_ptr<float>(new float[area], [](float *ptr) { delete[] ptr; }) };
            Image depth { width, height, std::shared_ptr<float>(new float[area], [](float *ptr) { delete[] ptr; }) };
            Image mask  { width, height, std::shared_ptr<float>(new float[area], [](float *ptr) { delete[] ptr; }) };
            PYR rotation; rotStream >> rotation.pitch >> rotation.roll >> rotation.yaw;

            for (uint32_t r = 0; r < height; ++r)
            {
                for (uint32_t c = 0; c < width; ++c)
                {
                    png::rgba_pixel    colorPixel = colorPng.get_pixel(c, r);
                    png::gray_pixel_16 depthPixel = depthPng.get_pixel(c, r);
                    png::gray_pixel_16 maskPixel  = maskPng.get_pixel(c, r);
                    unsigned char *rawColor = (unsigned char*) (color.data.get() + r * width + c);
                    *rawColor       = colorPixel.blue;
                    *(rawColor + 1) = colorPixel.green;
                    *(rawColor + 2) = colorPixel.red;
                    *(depth.data.get() + r * width + c) = depthPixel * (4500.f / 65535.f);
                    *(mask.data.get() + r * width + c) = maskPixel ? 1 : 0;
                }
            }

            eat(fx, fy, color, depth, mask, rotation, Config::SAMPLING_DEFAULT_STEP, outfile, index);
        }
    }
    catch (...)
    {}

    return 0;
}
