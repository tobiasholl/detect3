TEMPLATE = aux

SCRIPTS += \
    blenderer.py \
    graph-gen.py

for(script, SCRIPTS): SCRIPT_SOURCES += $${PWD}/$${script}

actual.commands     = $${QMAKE_COPY} $${SCRIPT_SOURCES} $${OUT_PWD}
actual.depends      = FORCE
PRE_TARGETDEPS      = actual
QMAKE_EXTRA_TARGETS = actual
QMAKE_CLEAN        += $${SCRIPTS}
