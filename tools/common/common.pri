# detect3 configuration
isEmpty(DETECT3_PATH) { DETECT3_PATH = /usr/local/include/detect3 }
INCLUDEPATH += $${DETECT3_PATH}
DEPENDPATH  += $${DETECT3_PATH}/config.hpp

# Depend on this part
DEPENDPATH  += $${DETECT3_PATH}/tools/common
INCLUDEPATH += $${DETECT3_PATH}/tools/common
