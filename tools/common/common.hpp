#ifndef COMMON_HPP
#define COMMON_HPP

#include <memory>

#include <config.hpp>

struct Image
{
    size_t width;
    size_t height;
    std::shared_ptr<float> data;
};

struct Patch
{
    constexpr static size_t FLOATS = Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE * 4 + 6;
    union
    {
        float raw[FLOATS];
        struct
        {
            float dx, dy, dz, pitch, yaw, roll;

            // Separated floating-point channels
            float r[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
            float g[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
            float b[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
            float d[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
        };
    };
};

#define VOTE_COMPONENT(name, x, y, z)        union { std::array<float, 3> name; struct { float x, y, z; }; }
struct Vote
{
    size_t objectID;

    VOTE_COMPONENT(translation, dx,    dy,    dz);
    VOTE_COMPONENT(rotation,    pitch, yaw,   roll);
};


#endif // COMMON_HPP
