#!/usr/bin/python3
# Constructs and trains the neural network used for feature extraction.

if __name__ != "__main__":
    raise ImportError("Cannot import graph-gen.py")

# Data format configuration (the C++ sources are authoritative here)
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
FLOAT_SIZE   = 4
PATCH_PIXELS = 32 * 32 * 4
MD_ENTRIES   = 6
PATCH_BYTES  = PATCH_PIXELS * FLOAT_SIZE
MD_BYTES     = MD_ENTRIES * FLOAT_SIZE
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

import argparse
import numpy as np
import os
import struct
import tensorflow as tf

def prelu(alpha):
    def _calc_prelu(x):
        factor = tf.get_variable(alpha,
                                 dtype=x.dtype,
                                 shape=x.get_shape()[-1:],
                                 initializer=tf.constant_initializer(0.0))
        return tf.nn.relu(x) + tf.multiply(factor, x - tf.abs(x)) * 0.5
    return _calc_prelu

def model(F):
    # Input layer [B, 32, 32, 4] (32x32 pixel RGBD image)
    input_layer = tf.placeholder(tf.float32, [None, 32, 32, 4], name="in_layer")
    
    # Layer #1: Convolution (5x5 PReLU x8) -> [B, 32, 32, 32]
    convolution1 = tf.layers.conv2d(inputs=input_layer,
                                    filters=32,
                                    kernel_size=[5, 5],
                                    padding="SAME",
                                    activation=prelu("prelu1"),
                                    name="convolution1")
    
    # Layer #2: Max-Pool (2x2 PReLU) -> [B, 16, 16, 32]
    maxpool2 = tf.layers.max_pooling2d(inputs=convolution1,
                                       pool_size=[2, 2],
                                       strides=2,
                                       name="maxpool2")
    
    # Layer #3: Convolution (5x5 PReLU x2) -> [B, 16, 16, 64] -> [B, 16384]
    convolution3 = tf.layers.conv2d(inputs=maxpool2,
                                    filters=64,
                                    kernel_size=[5, 5],
                                    padding="SAME",
                                    activation=prelu("prelu3"),
                                    name="convolution3")
    convolution3_flat = tf.reshape(tensor=convolution3,
                                   shape=[-1, 16 * 16 * 64],
                                   name="convolution3_flat")
                                    
    # Layer #4: Fully connected (PReLU) -> [B, 1024]
    dense4 = tf.layers.dense(inputs=convolution3_flat,
                             units=1024,
                             activation=prelu("prelu4"),
                             name="dense4")
    
    # Layer #5: Fully connected (PReLU) -> [B, F]
    layer5 = tf.layers.dense(inputs=dense4,
                             units=F,
                             activation=prelu("prelu5"),
                             name="layer5")
    
    # Layer #6: Fully connected (PReLU) -> [B, 1024]
    dense6 = tf.layers.dense(inputs=layer5,
                             units=1024,
                             activation=prelu("prelu6"),
                             name="dense6")
    
    # Layer #7: Fully connected (PReLU) -> [B, 16384] -> [B, 16, 16, 64]
    dense7 = tf.layers.dense(inputs=dense6,
                             units=16 * 16 * 64,
                             activation=prelu("prelu7"),
                             name="dense7")
    dense7_shaped = tf.reshape(tensor=dense7,
                               shape=[-1, 16, 16, 64],
                               name="dense7_shaped")
                               
    # Layer #8: Deconvolution (2x2 PReLU) -> [B, 32, 32, 64]
    deconvolution8 = tf.layers.conv2d_transpose(inputs=dense7_shaped,
                                                filters=64,
                                                kernel_size=[2, 2],
                                                strides=[2, 2],
                                                padding='SAME',
                                                activation=prelu("prelu8"),
                                                name="deconvolution8")
    
    # Layer #9: Convolution (5x5 PReLU) -> [B, 32, 32, 32]
    convolution9 = tf.layers.conv2d(inputs=deconvolution8,
                                    filters=32,
                                    kernel_size=[5, 5],
                                    padding="SAME",
                                    activation=prelu("prelu9"),
                                    name="convolution9")
    
    # Layer #10: Convolution (5x5 PReLU) -> [B, 32, 32, 4]
    convolution10 = tf.layers.conv2d(inputs=convolution9,
                                     filters=4,
                                     kernel_size=[5, 5],
                                     padding="SAME",
                                     activation=prelu("prelu10"),
                                     name="convolution10")

    # Feature layer (separate node)
    feature_layer = tf.reshape(tensor=layer5,
                               shape=[-1, F],
                               name="feature_layer")
    
    # Set output
    output = convolution10
    
    # Calculate cost function
    cost = tf.reduce_sum(tf.square(output - input_layer));
    
    return {'input':    input_layer,
            'output':   output,
            'features': feature_layer,
            'cost':     cost}
            
def ae_model(F):
    # Input layer [B, 32, 32, 4] (32x32 pixel RGBD image)
    input_layer = tf.placeholder(tf.float32, [None, 32, 32, 4], name="in_layer")
    
    input_layer_flat = tf.reshape(tensor=input_layer,
                                  shape=[-1, 32*32*4],
                                  name="input_flat")
    
    # Layer #1: Fully connected (tanh) -> [B, 1500]
    layer1 = tf.layers.dense(inputs=input_layer_flat,
                             units=1500,
                             activation=tf.tanh,
                             name="dense1")
                             
    # Layer #2: Fully connected (tanh) -> [B, 1000]
    layer2 = tf.layers.dense(inputs=layer1,
                             units=1000,
                             activation=tf.tanh,
                             name="dense2")
                             
    # Layer #3: Fully connected (tanh) -> [B, F]
    layer3 = tf.layers.dense(inputs=layer2,
                             units=F,
                             activation=tf.tanh,
                             name="layer3")

    # Layer #4: Fully connected (tanh) -> [B, 1000]
    layer4 = tf.layers.dense(inputs=layer3,
                             units=1000,
                             activation=tf.tanh,
                             name="dense4")
    
    # Layer #5: Fully connected (tanh) -> [B, 1500]
    layer5 = tf.layers.dense(inputs=layer4,
                             units=1500,
                             activation=tf.tanh,
                             name="dense5")
                             
    # Layer #6: Fully connected (tanh) -> [B, 32*32*4]
    layer6 = tf.layers.dense(inputs=layer5,
                             units=32*32*4,
                             activation=tf.tanh,
                             name="dense6")
    
    layer6_shaped = tf.reshape(tensor=layer6,
                               shape=[-1, 32, 32, 4],
                               name="dense6_shaped")

    # Feature layer (separate node)
    feature_layer = tf.reshape(tensor=layer3,
                               shape=[-1, F],
                               name="feature_layer")
    
    # Set output
    output = layer6_shaped
    
    # Calculate cost function
    cost = tf.reduce_sum(tf.square(output - input_layer));
    
    return {'input':    input_layer,
            'output':   output,
            'features': feature_layer,
            'cost':     cost}

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--small", help="Train the (smaller) non-convolutional AE instead.", action="store_true")
parser.add_argument("-f", "--features", help="Number of nodes in the feature layer", type=int, default=128)
parser.add_argument("--rate", help="Learning rate", type=float, default=0.1)
parser.add_argument("--batch-size", help="Training batch size", type=int, default=100)
parser.add_argument("--epochs", help="Training epochs", type=int, default=10)
parser.add_argument("input", help="The set of patches (generated by patch-gen) to be used as training data")
parser.add_argument("output", help="File to write the graph definition to")
args = parser.parse_args()

with tf.Session() as session:
    if args.small:
        autoencoder = ae_model(args.features)
    else:
        autoencoder = model(args.features)
        
    optimizer = tf.train.AdamOptimizer(args.rate).minimize(autoencoder['cost'])
    session.run(tf.global_variables_initializer())
    
    patches = []
    with open(args.input, "rb") as training_data:
        while training_data:
            training_data.read(MD_BYTES)
            data = training_data.read(PATCH_BYTES)
            if len(data) != PATCH_BYTES:
                break
            patch = np.frombuffer(data, np.float32)
            patches.append(np.reshape(patch, [32, 32, 4]))
    
    for epoch in range(args.epochs):
        for batch in range(len(patches) // args.batch_size):
            batch_data = np.array(patches[args.batch_size * batch : args.batch_size * (batch + 1)])
            session.run(optimizer, feed_dict={autoencoder['input']: batch_data})
        print("Epoch {}: {}".format(epoch, session.run(autoencoder['cost'], feed_dict={autoencoder['input']: batch_data})))

    tf.train.Saver().save(session, args.output)
    
