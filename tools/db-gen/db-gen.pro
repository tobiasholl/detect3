TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

# Tools
include(../common/common.pri)

# Libraries (TensorFlow, FLANN, Boost.Filesystem)
isEmpty(TENSORFLOW_LIB_PATH)     { TENSORFLOW_LIB_PATH     = /usr/local/lib }
isEmpty(TENSORFLOW_INCLUDE_PATH) { TENSORFLOW_INCLUDE_PATH = /usr/local/include/tf }
isEmpty(EIGEN3_INCLUDE_PATH)     { EIGEN3_INCLUDE_PATH     = /usr/include/eigen3 }
isEmpty(CUDA_INCLUDE_PATH)       { CUDA_INCLUDE_PATH       = /opt/cuda/include }
LIBS += -L$${TENSORFLOW_LIB_PATH} -ltensorflow_cc -lprotobuf -lflann -lboost_filesystem -lboost_system
INCLUDEPATH += $${TENSORFLOW_INCLUDE_PATH} \
               $${EIGEN3_INCLUDE_PATH} \
               $${CUDA_INCLUDE_PATH}

# db-gen
SOURCES += \
    db-gen.cpp
