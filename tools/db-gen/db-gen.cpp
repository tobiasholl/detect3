#include <fstream>
#include <iostream>

#include <tensorflow/core/public/session.h>
#include <tensorflow/core/protobuf/meta_graph.pb.h>

#include <flann/flann.hpp>

#include <boost/filesystem.hpp>

#include <common.hpp>

// Try to keep this file as synchronized as possible to detect3's featureextraction.hpp and matching.hpp
using IndexType = flann::Index<flann::L2<float>>;

// This tool builds a FLANN index and a voting map from a set of patches.
// It will automatically append to an existing dataset if it exists.
// You must specify the current object's ID as the first parameter
int main(int argc, char *argv[])
{
    // Parse arguments
    if (argc != 5) throw std::logic_error("Usage: db-gen OBJECT-ID PATCHES CHECKPOINT OUTPUT-PREFIX");
    int arg = 1;

    size_t objectID = static_cast<size_t>(std::atoll(argv[arg++]));
    std::string patchfile = argv[arg++];
    std::string chkpt     = argv[arg++];
    std::string outprefix = argv[arg++];

    // Check for existing voting map / index
    enum { APPEND, CREATE } mode;
    if (boost::filesystem::exists(boost::filesystem::path(outprefix + ".map")) && boost::filesystem::exists(boost::filesystem::path(outprefix + ".index")))
        mode = APPEND;
    else
        mode = CREATE;

    std::cout << "db-gen: Mode is " << (mode == APPEND ? "APPEND" : "CREATE") << "." << std::endl;

    // Tensorflow setup
    tensorflow::SessionOptions  options;
    tensorflow::MetaGraphDef    metagraph;
    tensorflow::Session        *session;

    // Set options
    options.config.set_intra_op_parallelism_threads(Config::TENSORFLOW_THREADS);
    options.config.set_inter_op_parallelism_threads(Config::TENSORFLOW_THREADS);
    options.config.mutable_gpu_options()->set_allow_growth(true);

    // Create a TensorFlow session and load the graph
    TF_CHECK_OK(tensorflow::NewSession(options, &session));
    TF_CHECK_OK(tensorflow::ReadBinaryProto(tensorflow::Env::Default(), chkpt + ".meta", &metagraph));
    TF_CHECK_OK(session->Create(metagraph.graph_def()));

    // Load the weights from the checkpoint
    tensorflow::Tensor checkpoint(tensorflow::DT_STRING, tensorflow::TensorShape());
    checkpoint.scalar<std::string>()() = chkpt;
    TF_CHECK_OK(session->Run({{ metagraph.saver_def().filename_tensor_name(), checkpoint },}, {}, { metagraph.saver_def().restore_op_name() }, nullptr));

    std::cout << "db-gen: Successfully loaded TensorFlow checkpoint." << std::endl;

    // Read patches
    std::vector<Patch> patches;
    std::ifstream patchstream(patchfile, std::ios_base::binary);
    while (patchstream)
    {
        Patch patch;
        patchstream.read(reinterpret_cast<char*>(patch.raw), Patch::FLOATS * sizeof(float));
        patches.push_back(std::move(patch));
    }

    std::cout << "db-gen: Read " << patches.size() << " patches." << std::endl;

    // Open voting map and prepare FLANN matrix
    std::fstream votingMap;
    if (mode == APPEND)
        votingMap = std::fstream(outprefix + ".map", std::ios_base::out | std::ios_base::in | std::ios_base::binary | std::ios_base::app);
    else
        votingMap = std::fstream(outprefix + ".map", std::ios_base::out | std::ios_base::binary);
    std::shared_ptr<float> rawData(new float[patches.size() * Config::FEATURE_EXTRACTION_DIMENSIONS], [](float *ptr){delete[] ptr;});
    flann::Matrix<float> dataset(rawData.get(), patches.size(), Config::FEATURE_EXTRACTION_DIMENSIONS);

    // Write number of votes in CREATE mode
    size_t voteCount = patches.size();
    if (mode == CREATE) votingMap.write(reinterpret_cast<char*>(&voteCount), sizeof(size_t));

    // Handle each patch
    for (size_t index = 0; index < patches.size(); ++index)
    {
        // Grab the patch
        Patch& patch = patches[index];

        // Build input tensor
        tensorflow::Tensor input(tensorflow::DT_FLOAT, tensorflow::TensorShape({Config::PATCH_PIXEL_SIZE, Config::PATCH_PIXEL_SIZE, 4}));
        std::copy_n(patch.r, Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE * 4, input.flat<float>().data());

        // Run the feature extraction step
        std::vector<tensorflow::Tensor> outputs;
        TF_CHECK_OK(session->Run({{ "in_layer", input },}, { "feature_layer", }, {}, &outputs));

        // Write features to FLANN matrix
        std::memcpy(rawData.get() + index * Config::FEATURE_EXTRACTION_DIMENSIONS, outputs[0].flat<float>().data(), Config::FEATURE_EXTRACTION_DIMENSIONS * sizeof(float));

        // Write metadata to the voting map
        Vote vote { objectID, patch.dx, patch.dy, patch.dz, patch.pitch, patch.yaw, patch.roll };
        votingMap.write(reinterpret_cast<char*>(&vote), sizeof(Vote));
    }

    std::cout << "db-gen: Extracted features from " << patches.size() << " patches." << std::endl;

    // Update number of votes in APPEND mode
    if (mode == APPEND)
    {
        votingMap.seekg(0);
        size_t oldCount;
        votingMap.read(reinterpret_cast<char*>(&oldCount), sizeof(size_t));
        voteCount += oldCount;
        votingMap.seekp(0);
        votingMap.write(reinterpret_cast<char*>(&voteCount), sizeof(size_t));
    }

    // (Re-)Build and save the FLANN index
    if (mode == APPEND)
    {
        IndexType index(flann::SavedIndexParams(outprefix + ".index"));
        index.addPoints(dataset, 1); // 'rebuild_threshold' set to 1 should automatically rebuild the index.
        std::cout << "db-gen: Built the index." << std::endl;
        index.save(outprefix + ".index");
    }
    else
    {
        IndexType index(dataset, flann::AutotunedIndexParams(0.8f, 0.01f, 0, 0.5f));
        index.buildIndex();
        std::cout << "db-gen: Built the index." << std::endl;
        index.save(outprefix + ".index");
    }
    std::cout << "db-gen: Saved the index." << std::endl;

    // Clean up
    session->Close();

    return 0;
}
