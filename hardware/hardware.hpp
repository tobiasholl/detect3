#ifndef HARDWARE_HPP
#define HARDWARE_HPP

#include <stdexcept>

namespace Hw
{

class Error : public std::runtime_error
{
public:
    template <typename T>
    Error(T&& t) : std::runtime_error(std::forward<T>(t))
    {}
};

}

#endif // HARDWARE_HPP
