#ifndef KINECT2_ACQUISITION_HPP
#define KINECT2_ACQUISITION_HPP

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/logger.h>
#include <libfreenect2/packet_pipeline.h>

#include "../../config.hpp"
#include "../../concurrency.hpp"
#include "../../log.hpp"
#include "../hardware.hpp"

#include "framepack.hpp"

namespace Hw
{
namespace Kinect2
{

// DiscardLogger: Discards the builtin libfreenect2 logs.

class DiscardLogger
    : public libfreenect2::Logger
{
public:
    void log(libfreenect2::Logger::Level, std::string const&) override {}
};


// LibraryServant: Lives in the AcquisitionServant's worker thread, responsible for accessing libfreenect2.

class LibraryServant
{
public:
    LibraryServant()
        : m_freenect2(new libfreenect2::Freenect2)
    {
        libfreenect2::setGlobalLogger(new DiscardLogger{});
        Log::log(Log::Debug, "libfreenect2 initialized.");
    }
    
    int enumerateDevices() { return m_freenect2->enumerateDevices(); }
    std::string getDeviceSerialNumber(int idx) { return m_freenect2->getDeviceSerialNumber(idx); }
    std::string getDefaultDeviceSerialNumber() { return m_freenect2->getDefaultDeviceSerialNumber(); }

    template <typename T>
    std::shared_ptr<libfreenect2::Freenect2Device> openDevice(T const& idxOrSerial, std::shared_ptr<libfreenect2::PacketPipeline> factory = nullptr)
    {
        if (factory)
            return std::shared_ptr<libfreenect2::Freenect2Device>(m_freenect2->openDevice(idxOrSerial, factory.get()));
        else
            return std::shared_ptr<libfreenect2::Freenect2Device>(m_freenect2->openDevice(idxOrSerial));
    }

    std::shared_ptr<libfreenect2::Freenect2Device> openDefaultDevice(std::shared_ptr<libfreenect2::PacketPipeline> factory = nullptr)
    {
        if (factory)
            return std::shared_ptr<libfreenect2::Freenect2Device>(m_freenect2->openDefaultDevice(factory.get()));
        else
            return std::shared_ptr<libfreenect2::Freenect2Device>(m_freenect2->openDefaultDevice());
    }

private:
    std::shared_ptr<libfreenect2::Freenect2> m_freenect2;
};

class LibraryProxy
    : public boost::asynchronous::servant_proxy<LibraryProxy, LibraryServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    LibraryProxy(Scheduler s)
        : boost::asynchronous::servant_proxy<LibraryProxy, LibraryServant, Concurrent::Job>(s)
    {}
    
    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Hw::Kinect2::LibraryProxy()",  Concurrent::HIGH_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Hw::Kinect2::~LibraryProxy()", Concurrent::NORMAL_PRIORITY)
    
    BOOST_ASYNC_FUTURE_MEMBER_LOG(enumerateDevices,             "Hw::Kinect2::LibraryProxy::enumerateDevices",             Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getDeviceSerialNumber,        "Hw::Kinect2::LibraryProxy::getDeviceSerialNumber",        Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getDefaultDeviceSerialNumber, "Hw::Kinect2::LibraryProxy::getDefaultDeviceSerialNumber", Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_MEMBER_UNSAFE_CALLBACK_LOG(openDevice,        "Hw::Kinect2::LibraryProxy::openDevice",        Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_MEMBER_UNSAFE_CALLBACK_LOG(openDefaultDevice, "Hw::Kinect2::LibraryProxy::openDefaultDevice", Concurrent::NORMAL_PRIORITY)
};


// FrameSequencer: An asynchronous drop-in replacement for libfreenect2's SyncMultiFrameListener

class FrameSequencer
    : public libfreenect2::FrameListener
{
public:
    bool onNewFrame(libfreenect2::Frame::Type type, libfreenect2::Frame *frame) override
    {
        // Unfortunately, the frame sequence IDs do not match for color and depth frames, so we cannot easily merge them by ID.
        // Instead, just send a FramePack as soon as it is filled.

        switch (type)
        {
            case libfreenect2::Frame::Type::Color: m_current.color.reset(frame); break;
            case libfreenect2::Frame::Type::Depth: m_current.depth.reset(frame); break;
            default: return false;
        }

        if (m_current.ready())
        {
            m_current.frameID = m_id++;
            m_consumer(FramePack(m_current));
            // To wait for new data to come in every time, simply use the following code instead. However, this drastically reduces framerate.
            //  m_consumer(std::move(m_current));
            //  m_current.cleanUp();
        }

        return true;
    }

    void setConsumer(FramePackConsumer consumer) { m_consumer = consumer; }
    void clear() { m_current.cleanUp(); }

private:
    size_t            m_id = 0;
    FramePack         m_current;
    FramePackConsumer m_consumer;
};


// DeviceServant: Lives in the AcquisitionServant's worker thread, responsible for managing a libfreenect2::Freenect2Device and receiving its frames

class DeviceServant
{
public:
    void setFramePackConsumer(FramePackConsumer consumer)
    {
        m_sequencer.setConsumer(std::move(consumer));
    }

    void setParameterConsumer(ParameterConsumer consumer)
    {
        m_parameterConsumer = consumer;
        if (m_device)
            transmitParameters();
    }

    void setDevice(std::shared_ptr<libfreenect2::Freenect2Device> device)
    {
        stop();
        m_device = device;
        m_device->setColorFrameListener(&m_sequencer);
        m_device->setIrAndDepthFrameListener(&m_sequencer);
        if (!m_device->start())
            throw Hw::Error("Could not start device.");
        transmitParameters();
        Log::log(Log::Debug, "Kinect 2 started.");
    }

    void transmitParameters()
    {
        m_parameterConsumer(m_device->getColorCameraParams(), m_device->getIrCameraParams());
    }

    void stop() noexcept
    {
        if (m_device)
        {
            m_device->stop();
            m_device->close();
        }
        m_sequencer.clear();
        Log::log(Log::Debug, "Kinect 2 stopped.");
    }

    ~DeviceServant()
    {
        stop();
    }
    
private:
    ParameterConsumer                              m_parameterConsumer;
    FrameSequencer                                 m_sequencer;
    std::shared_ptr<libfreenect2::Freenect2Device> m_device;
};

class DeviceProxy
    : public boost::asynchronous::servant_proxy<DeviceProxy, DeviceServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    DeviceProxy(Scheduler s)
        : boost::asynchronous::servant_proxy<DeviceProxy, DeviceServant, Concurrent::Job>(s)
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Hw::Kinect2::DeviceProxy()",  Concurrent::HIGH_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Hw::Kinect2::~DeviceProxy()", Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setParameterConsumer, "Hw::Kinect2::DeviceProxy::setParameterConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(setFramePackConsumer, "Hw::Kinect2::DeviceProxy::setFramePackConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(setDevice,            "Hw::Kinect2::DeviceProxy::setDevice",            Concurrent::NORMAL_PRIORITY)
};


// AcquisitionServant: Controls the image acquisition process.

class AcquisitionServant
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
private:
    auto onDeviceReady()
    {
        return this->make_safe_callback(
            [this](boost::asynchronous::expected<std::shared_ptr<libfreenect2::Freenect2Device>> result)
            {
                this->m_device.setDevice(result.get());
            },
            "Hw::Kinect2::AcquisitionServant / onDeviceReady", Concurrent::NORMAL_PRIORITY
        );
    }

public:
    AcquisitionServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(
              scheduler,
              Concurrent::createSingleThreadScheduler("Hw::Kinect2::AcquisitionServant")
          )
        , m_library(this->get_worker())
        , m_device(this->get_worker())
    {
        // Block here to ensure acquisition is not attempted before device enumeration is done.
        if (m_library.enumerateDevices().get() == 0)
            throw Hw::Error("No Kinect 2 found.");
    }

    template <typename T>
    void setNextStage(T const& nextStage)
    {
        m_device.setFramePackConsumer(nextStage.getFramePackConsumer().get());
        m_device.setParameterConsumer(nextStage.getParameterConsumer().get());
    }

    template <typename T>
    void startDevice(T const& idxOrSerial, std::shared_ptr<libfreenect2::PacketPipeline> factory = nullptr)
    {
        m_library.openDevice(onDeviceReady(), idxOrSerial, factory);
    }

    void startDefaultDevice(std::shared_ptr<libfreenect2::PacketPipeline> factory = nullptr)
    {
        m_library.openDefaultDevice(onDeviceReady(), factory);
    }

private:
    LibraryProxy m_library;
    DeviceProxy  m_device;
};

class AcquisitionProxy
    : public boost::asynchronous::servant_proxy<AcquisitionProxy, AcquisitionServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    AcquisitionProxy(Scheduler s)
        : boost::asynchronous::servant_proxy<AcquisitionProxy, AcquisitionServant, Concurrent::Job>(s)
    {}

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Hw::Kinect2::AcquisitionProxy()",  Concurrent::HIGH_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Hw::Kinect2::~AcquisitionProxy()", Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setNextStage,       "Hw::Kinect2::AcquisitionProxy::setNextStage",       Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(startDevice,        "Hw::Kinect2::AcquisitionProxy::startDevice",        Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(startDefaultDevice, "Hw::Kinect2::AcquisitionProxy::startDefaultDevice", Concurrent::NORMAL_PRIORITY)
};

} // namespace Kinect2
} // namespace Hw

#endif // KINECT2_ACQUISITION_HPP
