#ifndef KINECT2_REGISTRATION_HPP
#define KINECT2_REGISTRATION_HPP

#include <cstdint>
#include <memory>

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#include <libfreenect2/packet_pipeline.h>

#include "../../config.hpp"
#include "../../concurrency.hpp"
#include "../../log.hpp"
#include "../hardware.hpp"

#include "framepack.hpp"

namespace Hw
{
namespace Kinect2
{

// RegistrationServant: Controls the registration step immediately after acquisition

class RegistrationServant
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
public:
    RegistrationServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(
              scheduler,
              Concurrent::createThreadpool(Config::KINECT2_REGISTRATION_STAGE_THREADS, "Hw::Kinect2::RegistrationServant")
          )
    {}

    ParameterConsumer getParameterConsumer()
    {
        return this->make_safe_callback(
            [this](libfreenect2::Freenect2Device::ColorCameraParams colorParameters, libfreenect2::Freenect2Device::IrCameraParams irParameters)
            {
                Log::log(Log::Verbose, "RegistrationServant received device parameters:");
                Log::log(Log::Verbose, "  Color:");
                Log::log(Log::Verbose, "     cx      ", colorParameters.cx);
                Log::log(Log::Verbose, "     cy      ", colorParameters.cy);
                Log::log(Log::Verbose, "     fx      ", colorParameters.fx);
                Log::log(Log::Verbose, "     fy      ", colorParameters.fy);
                Log::log(Log::Verbose, "     mx_x0y0 ", colorParameters.mx_x0y0);
                Log::log(Log::Verbose, "     mx_x0y1 ", colorParameters.mx_x0y1);
                Log::log(Log::Verbose, "     mx_x0y2 ", colorParameters.mx_x0y2);
                Log::log(Log::Verbose, "     mx_x0y3 ", colorParameters.mx_x0y3);
                Log::log(Log::Verbose, "     mx_x1y0 ", colorParameters.mx_x1y0);
                Log::log(Log::Verbose, "     mx_x1y1 ", colorParameters.mx_x1y1);
                Log::log(Log::Verbose, "     mx_x1y2 ", colorParameters.mx_x1y2);
                Log::log(Log::Verbose, "     mx_x2y0 ", colorParameters.mx_x2y0);
                Log::log(Log::Verbose, "     mx_x2y1 ", colorParameters.mx_x2y1);
                Log::log(Log::Verbose, "     mx_x3y0 ", colorParameters.mx_x3y0);
                Log::log(Log::Verbose, "     shift_d ", colorParameters.shift_d);
                Log::log(Log::Verbose, "     shift_m ", colorParameters.shift_m);
                Log::log(Log::Verbose, "  IR:");
                Log::log(Log::Verbose, "     cx      ", irParameters.cx);
                Log::log(Log::Verbose, "     cy      ", irParameters.cy);
                Log::log(Log::Verbose, "     fx      ", irParameters.fx);
                Log::log(Log::Verbose, "     fy      ", irParameters.fy);
                Log::log(Log::Verbose, "     k1      ", irParameters.k1);
                Log::log(Log::Verbose, "     k2      ", irParameters.k2);
                Log::log(Log::Verbose, "     k3      ", irParameters.k3);
                Log::log(Log::Verbose, "     p1      ", irParameters.p1);
                Log::log(Log::Verbose, "     p2      ", irParameters.p2);

                this->m_registration.reset(new libfreenect2::Registration(irParameters, colorParameters));
                this->m_parameterConsumer(std::move(colorParameters), std::move(irParameters));
            },
            "Hw::Kinect2::RegistrationServant / getParameterConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    FramePackConsumer getFramePackConsumer()
    {
        return this->make_safe_callback(
            [this](FramePack&& pack)
            {
                this->eat(std::move(pack));
            },
            "Hw::Kinect2::RegistrationServant / getFramePackConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    template <typename T>
    void setNextStage(T const& nextStage)
    {
        m_framePackConsumer = nextStage.getFramePackConsumer().get();
        m_parameterConsumer = nextStage.getParameterConsumer().get();
    }

    void eat(FramePack&& pack)
    {
        auto registration = m_registration;

        if (!registration)
            throw Hw::Error("Attempted registration before parameters were transmitted");

        post_callback(
            [pack=std::move(pack), registration, consumer=m_framePackConsumer]() mutable
            {
                // Apply registration
                auto undistorted = std::make_shared<libfreenect2::Frame>(Config::KINECT2_IMAGE_WIDTH, Config::KINECT2_IMAGE_HEIGHT, 4);
                auto registered  = std::make_shared<libfreenect2::Frame>(Config::KINECT2_IMAGE_WIDTH, Config::KINECT2_IMAGE_HEIGHT, 4);

                registration->apply(pack.color.get(), pack.depth.get(), undistorted.get(), registered.get());

                // Send results. Note: Depth values range from 400..4500 (0.4 to 4.5 meters)
                consumer(FramePack { pack.frameID, registered, undistorted } );
            },
            [](boost::asynchronous::expected<void> result)
            {
                // Verify success.
                result.get();
            },
            "Hw::Kinect2::RegistrationServant::eat", Concurrent::NORMAL_PRIORITY, Concurrent::NORMAL_PRIORITY
        );
    }

private:
    std::shared_ptr<libfreenect2::Registration> m_registration;
    FramePackConsumer                           m_framePackConsumer;
    ParameterConsumer                           m_parameterConsumer;
};

class RegistrationProxy
    : public boost::asynchronous::servant_proxy<RegistrationProxy, RegistrationServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    RegistrationProxy(Scheduler s)
        : boost::asynchronous::servant_proxy<RegistrationProxy, RegistrationServant, Concurrent::Job>(s)
    {}

    BOOST_ASYNC_FUTURE_MEMBER_LOG(getParameterConsumer, "Hw::Kinect2::RegistrationProxy::getParameterConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getFramePackConsumer, "Hw::Kinect2::RegistrationProxy::getFramePackConsumer", Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setNextStage, "Hw::Kinect2::RegistrationProxy::setNextStage", Concurrent::NORMAL_PRIORITY)
};

} // namespace Kinect2
} // namespace Hw

#endif // KINECT2_REGISTRATION_HPP
