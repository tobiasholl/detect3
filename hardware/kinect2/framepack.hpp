#ifndef KINECT2_FRAMEPACK_HPP
#define KINECT2_FRAMEPACK_HPP

#include <cstdint>
#include <functional>
#include <memory>

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>

namespace Hw
{
namespace Kinect2
{

struct FramePack
{
    size_t                               frameID = 0;
    std::shared_ptr<libfreenect2::Frame> color   = nullptr;
    std::shared_ptr<libfreenect2::Frame> depth   = nullptr;

    void cleanUp()
    {
        frameID = 0;
        color.reset();
        depth.reset();
    }

    bool ready()
    {
        return color && depth;
    }
};

using FramePackConsumer = std::function<void(FramePack&&)>;
using ParameterConsumer = std::function<void(libfreenect2::Freenect2Device::ColorCameraParams, libfreenect2::Freenect2Device::IrCameraParams)>;

} // namespace Kinect2
} // namespace Hw

#endif // KINECT2_FRAMEPACK_HPP
