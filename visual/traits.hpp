#ifndef TRAITS_HPP
#define TRAITS_HPP

#include <opencv2/core.hpp>

#include "../hardware/kinect2/framepack.hpp"

namespace Vi
{

// Specialize ImageTraits for all raw image types.
// Specialization should implement:
//   static size_t  id(T const&)
//   static cv::Mat toColorMatrix(T const&)
//   static cv::Mat toDepthMatrix(T const&)
template <typename T> struct ImageTraits;

template <> struct ImageTraits<Hw::Kinect2::FramePack>
{
    using T = Hw::Kinect2::FramePack;

    static size_t id(T const& image)
    {
        return image.frameID;
    }

    static cv::Mat toColorMatrix(T const& image)
    {
        libfreenect2::Frame *frame = image.color.get();
        cv::Mat img(frame->height, frame->width, CV_8UC3);

        unsigned char *raw = frame->data;
        for (size_t row = 0; row < frame->height; ++row)
            for (size_t col = 0; col < frame->width; ++col, raw += 4)
                img.at<cv::Vec3b>(row, col) = cv::Vec3b(raw[0], raw[1], raw[2]);

        return img;
    }

    static cv::Mat toDepthMatrix(T const& image)
    {
        libfreenect2::Frame *frame = image.depth.get();
        cv::Mat img(frame->height, frame->width, CV_32FC1);

        float *raw = reinterpret_cast<float*>(frame->data);
        for (size_t row = 0; row < frame->height; ++row)
            for (size_t col = 0; col < frame->width; ++col, ++raw)
                img.at<float>(row, col) = *raw;

        return img;
    }
};

} // namespace Vi

#endif // TRAITS_HPP
