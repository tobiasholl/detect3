#ifndef VISUALIZATION_HPP
#define VISUALIZATION_HPP

#include <functional>
#include <map>
#include <optional>
#include <tuple>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include "../config.hpp"
#include "../concurrency.hpp"
#include "../log.hpp"

#include "../pipeline/types.hpp"
#include "traits.hpp"

namespace Vi
{

#define CACHE_ENTRY_UNDERLYING_TYPE std::tuple<std::optional<Ts>...>
template <typename... Ts>
class CacheEntry : CACHE_ENTRY_UNDERLYING_TYPE
{
public:
    constexpr static size_t size = std::tuple_size_v<CACHE_ENTRY_UNDERLYING_TYPE>;

    bool ready() const { return _ready<0>(); }
    void extract(Ts&... targets) { _extract<0, Ts...>(targets...); }

    template <typename T>           void set(T&& t) { std::get<std::optional<T>>(*this) = std::move(t); }
    template <size_t N, typename T> void set(T&& t) { std::get<N>(*this) = std::move(t); }

private:
    template <size_t N> bool _ready() const
    {
        if constexpr (N < size - 1) return std::get<N>(*this).has_value() && _ready<N + 1>();
        else                        return std::get<N>(*this).has_value();
    }

    template <size_t N, typename T, typename... Rs> void _extract(T& t, Rs&... rs)
    {
        t = std::get<N>(*this).value();
        if constexpr (N < size - 1) _extract<N + 1, Rs...>(rs...);
    }
};
#undef CACHE_ENTRY_UNDERLYING_TYPE

template <typename ImageType>
class GUIServant
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
private:
    using Traits = ImageTraits<ImageType>;

public:
    GUIServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(scheduler) // No worker threadpool
    {}

    Pl::MultiVoteConsumer getMultiVoteConsumer()
    {
        return this->make_safe_callback(
            [this](size_t id, std::vector<Pl::Vote>&& votes)
            {
                this->eatVotes(id, std::move(votes));
            },
            "Vi::GUIServant / getMultiVoteConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    std::function<void(ImageType&&)> getImageConsumer()
    {
        return this->make_safe_callback(
            [this](ImageType&& image)
            {
                this->eatImage(std::move(image));
            },
            "Vi::GUIServant / getImageConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    Pl::ParameterConsumer getParameterConsumer()
    {
        return this->make_safe_callback(
            [this](Pl::CameraParameters const& params)
            {
                this->m_params = params;
            },
            "Vi::GUIServant / getParameterConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    void eatVotes(size_t id, std::vector<Pl::Vote>&& votes)
    {
        m_cache[id].template set<std::vector<Pl::Vote>>(std::move(votes));
        if (m_cache[id].ready()) this->ready(id);
    }

    void eatImage(ImageType&& image)
    {
        size_t id = Traits::id(image);
        m_cache[id].template set<ImageType>(std::move(image));
        if (m_cache[id].ready()) this->ready(id);
    }

    void ready(size_t id)
    {
        ImageType             image;
        std::vector<Pl::Vote> votes;
        m_cache[id].extract(image, votes);

        cv::Mat matrix = Traits::toColorMatrix(image);
        for (auto const& vote : votes)
        {
            size_t row = round(vote.dy / vote.dz * m_params.fy + m_params.cy);
            size_t col = round(vote.dx / vote.dz * m_params.fx + m_params.cx);
            matrix.at<cv::Vec3b>(row, col) = cv::Vec3b(0, 0, 255);
        }
        cv::namedWindow("A");
        cv::imshow("A", matrix);
        cv::waitKey(1);
    }

private:
    Pl::CameraParameters m_params;
    std::map<size_t, CacheEntry<ImageType, std::vector<Pl::Vote>>> m_cache;
};

template <typename ImageType>
class GUIProxy
    : public boost::asynchronous::servant_proxy<GUIProxy<ImageType>, GUIServant<ImageType>, Concurrent::Job>
{
public:
    template <typename Scheduler>
    GUIProxy(Scheduler s)
        : boost::asynchronous::servant_proxy<GUIProxy<ImageType>, GUIServant<ImageType>, Concurrent::Job>(s)
    {}

    // C++ standard (§ 14.6.2/3) workaround
    using base_type = boost::asynchronous::servant_proxy<GUIProxy<ImageType>, GUIServant<ImageType>, Concurrent::Job>;
    using callable_type = typename base_type::callable_type;

    BOOST_ASYNC_FUTURE_MEMBER_LOG(getParameterConsumer, "Vi::GUIProxy::getParameterConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getMultiVoteConsumer, "Vi::GUIProxy::getMultiVoteConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getImageConsumer,     "Vi::GUIProxy::getImageConsumer",     Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Vi::GUIProxy()",  Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Vi::~GUIProxy()", Concurrent::NORMAL_PRIORITY)
};

} // namespace Vi

#endif // VISUALIZATION_HPP
