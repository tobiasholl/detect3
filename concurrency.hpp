#ifndef CONCURRENCY_HPP
#define CONCURRENCY_HPP

#include <boost/asynchronous/any_shared_scheduler_proxy.hpp>
#include <boost/asynchronous/queue/lockfree_queue.hpp>
#include <boost/asynchronous/scheduler/single_thread_scheduler.hpp>
#include <boost/asynchronous/scheduler/threadpool_scheduler.hpp>
#include <boost/asynchronous/scheduler_shared_proxy.hpp>
#include <boost/asynchronous/servant_proxy.hpp>
#include <boost/asynchronous/trackable_servant.hpp>

namespace Concurrent
{

// Threading constants
using Job = BOOST_ASYNCHRONOUS_DEFAULT_JOB; //TODO: Use boost::asynchronous::any_loggable;

constexpr int LOWEST_PRIORITY = 99;
constexpr int NORMAL_PRIORITY =  2;
constexpr int HIGH_PRIORITY   =  1;
constexpr int ANY_PRIORITY    =  0;

// Threadpool and scheduler creation

//TODO: register for diagnostics (and regularly fetch them, otherwise the diagnostics storage fills up)
//TODO: add a second queue if you ever use HIGH_PRIORITY
auto createThreadpool(int threads, std::string const& name)
{
    auto scheduler = boost::asynchronous::make_shared_scheduler_proxy<
        boost::asynchronous::threadpool_scheduler<
            boost::asynchronous::lockfree_queue<Concurrent::Job>
        >
    >(threads, name);

    return scheduler;
}

auto createSingleThreadScheduler(std::string const& name)
{
    auto scheduler = boost::asynchronous::make_shared_scheduler_proxy<
        boost::asynchronous::single_thread_scheduler<
            boost::asynchronous::lockfree_queue<Concurrent::Job>
        >
    >(name);

    return scheduler;
}

}

#endif // CONCURRENCY_HPP
