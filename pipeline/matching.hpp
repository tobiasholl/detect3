#ifndef MATCHING_HPP
#define MATCHING_HPP

#include <fstream>
#include <vector>

#include <flann/flann.hpp>

#include "../config.hpp"
#include "../concurrency.hpp"
#include "../log.hpp"

#include "types.hpp"

namespace Pl
{

class VotingMap : public std::vector<Vote>
{
public:
    // Reads a binary (index -> vote) mapping (which is really just an array of votes dumped in binary format)
    VotingMap(std::string const& path)
    {
        // Open the file
        std::ifstream reader(path, std::ios_base::binary);
        char elementBuffer[sizeof(Vote)];

        // Read the number of votes
        reader.read(elementBuffer, sizeof(size_t));
        size_t count = *reinterpret_cast<size_t*>(elementBuffer);
        this->reserve(count);

        // Read 'count' votes from the stream
        for (size_t index = 0; index < count; ++index)
        {
            reader.read(elementBuffer, sizeof(Vote));
            this->push_back(*reinterpret_cast<Vote*>(elementBuffer));
        }
    }
};

class MatchingServant
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
private:
    using IndexType = flann::Index<flann::L2<float>>;
public:
    MatchingServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler, std::string const& codebookPrefix)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(
              scheduler,
              Concurrent::createThreadpool(Config::MATCHING_STAGE_THREADS, "Pl::MatchingServant")
          )
        , m_index(new IndexType(flann::SavedIndexParams(codebookPrefix + ".index")))
        , m_map(new VotingMap(codebookPrefix + ".map"))
    {
        m_params.cores = Config::FLANN_THREADS;
    }

    FeatureConsumer getFeatureConsumer()
    {
        return this->make_safe_callback(
            [this](Features&& features)
            {
                this->eat(std::move(features));
            },
            "Pl::MatchingServant / getFeatureConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    ParameterConsumer getParameterConsumer()
    {
        return this->make_safe_callback(
            [this](CameraParameters const& params)
            {
                this->m_paramConsumer(params);
            },
            "Pl::MatchingServant / getParameterConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    FlushConsumer getFlushConsumer()
    {
        return this->make_safe_callback(
            [this](size_t id)
            {
                this->m_flush(id);
            },
            "Pl::MatchingServant / getFlushConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    template <typename T>
    void setNextStage(T const& nextStage)
    {
        m_consumer      = nextStage.getVoteConsumer().get();
        m_paramConsumer = nextStage.getParameterConsumer().get();
        m_flush         = nextStage.getFlushConsumer().get();
    }

    void eat(Features&& features)
    {
        post_callback(
            [features = std::move(features), consumer = m_consumer, index = m_index, params = m_params, map = m_map]() mutable
            {
                // Perform k-nearest-neighbors search
                flann::Matrix<float>             query(features.raw(), 1, Config::FEATURE_EXTRACTION_DIMENSIONS);
                std::vector<std::vector<size_t>> indices;
                std::vector<std::vector<float>>  distances;
                index->knnSearch(query, indices, distances, Config::MATCHING_NEIGHBORS, params);

                // Process the resulting indices into votes ('indices' and 'distances' are both organized per query, but only one query is sent, therefore [0])
                for (size_t index = 0; index < indices[0].size(); ++index)
                    consumer(PackedVote { features.metadata, distances[0][index], map->operator[](indices[0][index]) });
            },
            [](boost::asynchronous::expected<void> const& result)
            {
                // Check that everything went well
                result.get();
            },
            "Pl::MatchingServant::eat",
            Concurrent::NORMAL_PRIORITY,
            Concurrent::LOWEST_PRIORITY
        );
    }

private:
    PackedVoteConsumer         m_consumer;
    ParameterConsumer          m_paramConsumer;
    FlushConsumer              m_flush;
    std::shared_ptr<IndexType> m_index;
    flann::SearchParams        m_params;
    std::shared_ptr<VotingMap> m_map;
};

class MatchingProxy
    : public boost::asynchronous::servant_proxy<MatchingProxy, MatchingServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    MatchingProxy(Scheduler s, std::string const& codebookPrefix)
        : boost::asynchronous::servant_proxy<MatchingProxy, MatchingServant, Concurrent::Job>(s, codebookPrefix)
    {}

    BOOST_ASYNC_FUTURE_MEMBER_LOG(getFeatureConsumer,   "Pl::MatchingProxy::getFeatureConsumer",   Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getParameterConsumer, "Pl::MatchingProxy::getParameterConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getFlushConsumer,     "Pl::MatchingProxy::getFlushConsumer",     Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setNextStage,   "Pl::MatchingProxy::setNextStage",   Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Pl::MatchingProxy()",  Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Pl::~MatchingProxy()", Concurrent::NORMAL_PRIORITY)
};

} // namespace Pl

#endif // MATCHING_HPP
