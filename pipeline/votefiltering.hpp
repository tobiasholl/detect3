#ifndef VOTEFILTERING_HPP
#define VOTEFILTERING_HPP

#if (defined __AVX__)
    #include "immintrin.h"
    #define VEC_ENABLED
    #define VEC_SIZE    8
    #define VEC_TYPE    __m256
    #define VEC_LOAD    _mm256_loadu_ps
    #define VEC_FILL    _mm256_set1_ps
    #define VEC_MUL     _mm256_mul_ps
    #define VEC_ADD     _mm256_add_ps
    #define VEC_WRITE   _mm256_storeu_ps
#elif (defined __SSE__)
    #include "xmmintrin.h"
    #define VEC_ENABLED
    #define VEC_SIZE    4
    #define VEC_TYPE    __m128
    #define VEC_LOAD    _mm_loadu_ps
    #define VEC_FILL    _mm_set1_ps
    #define VEC_MUL     _mm_mul_ps
    #define VEC_ADD     _mm_add_ps
    #define VEC_WRITE   _mm_storeu_ps
#endif

#include <algorithm>
#include <limits>
#include <map>
#include <memory>
#include <vector>

#include "../config.hpp"
#include "../concurrency.hpp"
#include "../log.hpp"

#include "types.hpp"

namespace Pl
{

struct VoteDensityMap
{
    size_t width;
    size_t height;

    std::vector<float>             weights;
    std::vector<std::vector<Vote>> votes;

    VoteDensityMap(size_t vdmWidth, size_t vdmHeight) : width(vdmWidth), height(vdmHeight), weights(vdmWidth * vdmHeight), votes(vdmWidth * vdmHeight) {}

    void add(size_t row, size_t col, float weight, Vote&& vote)
    {
        size_t index = row * width + col;
        weights[index] += weight;
        votes[index].emplace_back(std::move(vote));
    }

    void blur()
    {
        // Gaussian blur, 3x3.
        // Kernel is
        //   1 2 1
        //   2 4 2    /16
        //   1 2 1

        // In general, this is way too small to parallelize
        // The margins of the output are zeroed

        std::vector<float> output(width * height);

#ifdef VEC_ENABLED
        VEC_TYPE two  = VEC_FILL(2.f);
        VEC_TYPE four = VEC_FILL(4.f);
        for (size_t index = width + 1; index < width * (height - 1) - 1; index += VEC_SIZE)
        {
            VEC_TYPE r1c1 =         VEC_LOAD(&weights[index - width - 1]);
            VEC_TYPE r1c2 = VEC_MUL(VEC_LOAD(&weights[index - width]),     two);
            VEC_TYPE r1c3 =         VEC_LOAD(&weights[index - width + 1]);
            VEC_TYPE r2c1 = VEC_MUL(VEC_LOAD(&weights[index - 1]),         two);
            VEC_TYPE r2c2 = VEC_MUL(VEC_LOAD(&weights[index]),             four);
            VEC_TYPE r2c3 = VEC_MUL(VEC_LOAD(&weights[index + 1]),         two);
            VEC_TYPE r3c1 =         VEC_LOAD(&weights[index + width - 1]);
            VEC_TYPE r3c2 = VEC_MUL(VEC_LOAD(&weights[index + width]),     two);
            VEC_TYPE r3c3 =         VEC_LOAD(&weights[index + width + 1]);

            VEC_TYPE c1 = VEC_ADD(r1c1, VEC_ADD(r2c1, r3c1));
            VEC_TYPE c2 = VEC_ADD(r1c2, VEC_ADD(r2c2, r3c2));
            VEC_TYPE c3 = VEC_ADD(r1c3, VEC_ADD(r2c3, r3c3));

            VEC_TYPE result = VEC_ADD(c1, VEC_ADD(c2, c3));

            VEC_WRITE(&output[index], result);
        }

        // Zero left, right, and bottom margin (top is already zeroed, bottom has at most VEC_SIZE - 2 garbage bytes)
        for (size_t row = 1; row < height - 1; ++row) output[row * width] = output[row * width + width - 1] = 0;
        std::memset(&output[(height - 1) * width], 0, (VEC_SIZE - 2) * sizeof(float));
#else
        for (size_t row = 1; row < height - 1; ++row)
            for (size_t col = 1; col < width - 1; ++col)
                output[row * width + col] = (weights[(row - 1) * width + col - 1] * 1.f +
                                             weights[(row - 1) * width + col]     * 2.f +
                                             weights[(row - 1) * width + col + 1] * 1.f +
                                             weights[row       * width + col - 1] * 2.f +
                                             weights[row       * width + col]     * 4.f +
                                             weights[row       * width + col + 1] * 2.f +
                                             weights[(row + 1) * width + col - 1] * 1.f +
                                             weights[(row + 1) * width + col]     * 2.f +
                                             weights[(row + 1) * width + col + 1] * 1.f) / 16.f;
#endif

        // Reassign the weights
        weights = std::move(output);
    }
};

// Internal stuff
namespace detail
{

float squareDistance(Vote const& a, Vote const& b)
{
    float ddx = a.dx - b.dx;
    float ddy = a.dy - b.dy;
    float ddz = a.dz - b.dz;
    return ddx * ddx + ddy * ddy + ddz * ddz;
}

template <typename T> constexpr T deg2rad(T degs) { return degs * M_PI / 180; }

float rotationDistance(Vote const& a, Vote const& b)
{
    // The rotation matrix from roll γ, pitch β, and yaw α is
    //
    //        cosβcosγ                  sinβcosγ - cosβsinγ       sinβ
    //    R = cosαsinγ + sinαsinβcosγ   cosαcosγ - sinαsinβsinγ   - sinαcosβ
    //        sinαsinγ - cosαsinβcosγ   sinαcosγ - cosαsinβsinγ   cosαcosβ
    //
    // and we know that tr(R) = 1 + 2cosθ (cf. Wikipedia)
    //
    // Then, tr(R) = cosαcosβ + cosαcosγ + cosβcosγ - sinαsinβsinγ
    // and the rotational angle we seek is θ = arccos((tr(R) - 1) / 2)

    float alpha = a.yaw - b.yaw;
    float beta  = a.pitch - b.pitch;
    float gamma = a.roll - b.roll;

    float trace = std::cos(alpha) * std::cos(beta)  +
                  std::cos(alpha) * std::cos(gamma) +
                  std::cos(beta)  * std::cos(gamma) -
                  std::sin(alpha) * std::sin(beta) * std::sin(gamma);

    return std::acos((trace - 1) / 2);
}

Vote singleMeanShift(Vote estimate, std::vector<Vote> const& votes, std::function<float(Vote const&, Vote const&)> distance, float threshold)
{
    float step;
    do
    {
        Vote   nextEstimate = {};
        size_t totalWeights = 0;
        for (Vote const& vote : votes)
        {
            size_t weight = distance(vote, estimate) > threshold ? 1 : 0;
            nextEstimate += vote * weight;
            totalWeights += weight;
        }
        nextEstimate /= totalWeights;
        step = distance(nextEstimate, estimate);
        estimate = nextEstimate;
    } while (step > Config::VOTE_FILTERING_CONVERGENCE);

    return estimate;
}

Vote meanShift(std::vector<Vote> const& votes)
{
    // Compute mean vote with the most voted-for objectID
    Vote mean = {};
    std::map<size_t, size_t> objectMap;
    for (Vote const& vote : votes)
    {
        mean += vote;
        objectMap[vote.objectID]++;
    }
    mean /= votes.size();
    mean.objectID = std::max_element(objectMap.begin(), objectMap.end(), [](auto const& a, auto const& b) { return a.second < b.second; })->first;

    // Mean shift for position and rotation separately
    Vote position = singleMeanShift(mean, votes, squareDistance,   Config::VOTE_FILTERING_LOCATION_KERNEL * Config::VOTE_FILTERING_LOCATION_KERNEL);
    Vote rotation = singleMeanShift(mean, votes, rotationDistance, deg2rad(Config::VOTE_FILTERING_ROTATION_KERNEL));

    // Update the mean vote
    mean.translation = position.translation;
    mean.rotation    = rotation.rotation;

    return mean;
}

} // namespace detail

class VoteFilteringServant
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
public:
    VoteFilteringServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(
              scheduler,
              Concurrent::createThreadpool(Config::VOTE_FILTERING_STAGE_THREADS, "Pl::VoteFilteringServant")
          )
    {}

    PackedVoteConsumer getVoteConsumer()
    {
        return this->make_safe_callback(
            [this](PackedVote&& vote)
            {
                this->eat(std::move(vote));
            },
            "Pl::VoteFilteringServant / getVoteConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    ParameterConsumer getParameterConsumer()
    {
        return this->make_safe_callback(
            [this](CameraParameters const& params)
            {
                this->m_params = params;
                this->m_paramConsumer(params);
            },
            "Pl::VoteFilteringServant / getParameterConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    FlushConsumer getFlushConsumer()
    {
        return this->make_safe_callback(
            [this](size_t id)
            {
                this->flush(id);
            },
            "Pl::VoteFilteringServant / getFlushConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    template <typename T>
    void setNextStage(T const& nextStage)
    {
        m_consumer      = nextStage.getMultiVoteConsumer().get();
        m_paramConsumer = nextStage.getParameterConsumer().get();
    }

    void eat(PackedVote&& vote)
    {
        Log::log(Log::Debug, "Received vote for frame ID ", vote.metadata.frameID);

        // Apply the vote translation
        float x = vote.vote.dx + vote.metadata.x;
        float y = vote.vote.dy + vote.metadata.y;
        float z = vote.vote.dz + vote.metadata.z;

        // Project back into the image
        float offsetRow = y / z * m_params.fy + m_params.cy;
        float offsetCol = x / z * m_params.fx + m_params.cx;

        // Build indices
        int indexRow = std::round(offsetRow / Config::VOTE_FILTERING_CELL_SIZE);
        int indexCol = std::round(offsetCol / Config::VOTE_FILTERING_CELL_SIZE);

        // Verify indices
        int vdmWidth  = m_params.width  / Config::VOTE_FILTERING_CELL_SIZE;
        int vdmHeight = m_params.height / Config::VOTE_FILTERING_CELL_SIZE;
        if (indexRow < 0 || indexRow >= vdmHeight || indexCol < 0 || indexCol >= vdmWidth)
        {
            Log::log(Log::Error, "On frame ", vote.metadata.frameID, ": Vote outside of image (in VDM cell [", indexRow, ", ", indexCol, "]).");
            return;
        }

        // Get the frame's VoteDensityMap (or create a new one, if necessary)
        auto iterator = m_density.find(vote.metadata.frameID);
        if (iterator == m_density.end())
            iterator = m_density.emplace(vote.metadata.frameID, std::make_shared<VoteDensityMap>(vdmWidth, vdmHeight)).first;

        // Insert the (absolute) vote
        iterator->second->add(indexRow, indexCol, vote.weight, Vote { vote.vote.objectID, { x, y, z }, vote.vote.rotation });
    }

    void flush(size_t id)
    {
        Log::log(Log::Debug, "Flushing votes for frame ID ", id);

        // If no votes for this ID have arrived yet, ignore the flush event.
        if (m_density.find(id) == m_density.end())
        {
            Log::log(Log::Error, "Flushing frame ", id, " before votes have arrived.");
            return;
        }

        // Grab the VoteDensityMap pointer
        auto vdm = m_density[id];

        post_callback(
            [id, vdm, consumer=m_consumer]() mutable
            {
                // Blur the weights
                vdm->blur();

                // Get the Config::VOTE_FILTERING_CANDIDATES highest peaks
                // I would love to do this with parallel_nth_element, but that
                // would just sort the weights, not the associated vote lists.
                // Because generally, the number of entries is not too large,
                // it is fine to do this in sequence.

                // Type aliases
                using Element  = std::pair<float, size_t>;

                // 'peaks' will maintain a min-heap of current peaks.
                std::vector<Element> peaks;
                std::greater<Element> comparison;
                float threshold = std::numeric_limits<float>::min();

                for (size_t index = 0; index < vdm->weights.size(); ++index)
                {
                    float weight = vdm->weights[index];
                    if (weight > threshold)
                    {
                        // The new value is greater than the threshold, and qualifies for inclusion as a peak.
                        peaks.emplace_back(weight, index);
                        std::push_heap(peaks.begin(), peaks.end(), comparison);
                    }
                    if (peaks.size() > Config::VOTE_FILTERING_CANDIDATES)
                    {
                        // Too many peaks, drop the lowest one and use it as the new threshold.
                        std::pop_heap(peaks.begin(), peaks.end(), comparison);
                        threshold = peaks.back().first;
                        peaks.pop_back();
                    }
                }

                // For each peak, collect its neighbors' votes
                std::vector<std::vector<Vote>> peakVotes;
                for (auto const& peak : peaks)
                {
                    std::vector<Vote> thisPeakVotes = vdm->votes[peak.second];
                    // Collect direct neighbors
                    if (peak.second > vdm->width)                     thisPeakVotes.insert(thisPeakVotes.end(), vdm->votes[peak.second - vdm->width].begin(), vdm->votes[peak.second - vdm->width].end());
                    if (peak.second % vdm->width > 0)                 thisPeakVotes.insert(thisPeakVotes.end(), vdm->votes[peak.second - 1].begin(),          vdm->votes[peak.second - 1].end());
                    if (peak.second % vdm->width < vdm->width - 1)    thisPeakVotes.insert(thisPeakVotes.end(), vdm->votes[peak.second + 1].begin(),          vdm->votes[peak.second + 1].end());
                    if (peak.second < vdm->width * (vdm->height - 1)) thisPeakVotes.insert(thisPeakVotes.end(), vdm->votes[peak.second + vdm->width].begin(), vdm->votes[peak.second + vdm->width].end());

                    peakVotes.push_back(std::move(thisPeakVotes));
                }

                // On the collected votes of each peak, perform mean shift
                std::vector<Vote> candidates;
                for (auto& peak : peakVotes)
                    candidates.push_back(detail::meanShift(peak));

                // Pass the candidates on
                consumer(id, std::move(candidates));
            },
            [this, vdm /* <= Keep alive */, id](boost::asynchronous::expected<void> const& result)
            {
                // Check that everything went well, then clean up the VDM map
                result.get();
                this->m_density.erase(m_density.find(id));
            },
            "Pl::VoteFilteringServant::eat",
            Concurrent::NORMAL_PRIORITY,
            Concurrent::LOWEST_PRIORITY
        );
    }

private:
    MultiVoteConsumer m_consumer;
    ParameterConsumer m_paramConsumer;
    CameraParameters  m_params;
    std::map<size_t, std::shared_ptr<VoteDensityMap>> m_density;
};

class VoteFilteringProxy
    : public boost::asynchronous::servant_proxy<VoteFilteringProxy, VoteFilteringServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    VoteFilteringProxy(Scheduler s)
        : boost::asynchronous::servant_proxy<VoteFilteringProxy, VoteFilteringServant, Concurrent::Job>(s)
    {}

    BOOST_ASYNC_FUTURE_MEMBER_LOG(getVoteConsumer,      "Pl::VoteFilteringProxy::getVoteConsumer",      Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getParameterConsumer, "Pl::VoteFilteringProxy::getParameterConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getFlushConsumer,     "Pl::VoteFilteringProxy::getFlushConsumer",     Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setNextStage,   "Pl::VoteFilteringProxy::setNextStage",   Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Pl::VoteFilteringProxy()",  Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Pl::~VoteFilteringProxy()", Concurrent::NORMAL_PRIORITY)
};

} // namespace Pl

#endif // VOTEFILTERING_HPP
