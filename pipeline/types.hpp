#ifndef TYPES_HPP
#define TYPES_HPP

#include <functional>

#include "../config.hpp"

namespace Pl
{

// Metadata: Information about a patch/feature/vote that is carried through the different stages
struct Metadata
{
    size_t frameID; // ID of the frame this patch belongs to
    size_t row;     // Row of the sample's center pixel
    size_t col;     // Column of the sample's center pixel
    float  x;       // X coordinate (in meters) of the sample's center
    float  y;       // Y coordinate (in meters) of the sample's center
    float  z;       // Z coordinate / depth (in meters) of the sample's center
};

// CameraParameters: Camera parameters for the registered image
struct CameraParameters
{
    size_t width;   // Image width
    size_t height;  // Image height
    float  fx;      // Camera's focal length along the X axis
    float  fy;      // Camera's focal length along the Y axis
    float  cx;      // Camera center point, X axis
    float  cy;      // Camera center point, Y axis
};

// Patch: An RGBD patch ready from sampling ready for the CNN
struct Patch
{
    // Metadata
    Metadata metadata;

    // Separated floating-point channels
    float r[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
    float g[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
    float b[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];
    float d[Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE];

    // Access to the raw data
    float       *raw()       { return &r[0]; }
    const float *raw() const { return &r[0]; }
};

// Features: The features extracted from a patch
struct Features
{
    Metadata metadata;
    std::array<float, Config::FEATURE_EXTRACTION_DIMENSIONS> content;

    // Access to the raw data
    float       *raw()       { return content.data(); }
    const float *raw() const { return content.data(); }

    // Construct from just metadata
    Features(Metadata const& meta) : metadata(meta) {}
};

// Vote: A 6D positional vote alongside an object ID vote
#define VOTE_COMPONENT(name, x, y, z)        union { std::array<float, 3> name; struct { float x, y, z; }; }
#define VOTE_OPERATOR_MODIF(name, op)        Vote& name(Vote const& v) { dx op v.dx; dy op v.dy; dz op v.dz; pitch op v.pitch; yaw op v.yaw; roll op v.roll; return *this; }
#define VOTE_OPERATOR_MODIF_SCALAR(name, op) template <typename T> Vote& name(T const& t) { dx op t; dy op t; dz op t; pitch op t; yaw op t; roll op t; return *this; }
#define VOTE_OPERATOR_SCALAR(name, op)       template <typename T> Vote name(T const& t) const { return Vote { objectID, dx op t, dy op t, dz op t, pitch op t, yaw op t, roll op t }; }
struct Vote
{
    size_t objectID;

    VOTE_COMPONENT(translation, dx,    dy,    dz);
    VOTE_COMPONENT(rotation,    pitch, yaw,   roll);

    VOTE_OPERATOR_MODIF(operator+=, +=)
    VOTE_OPERATOR_MODIF(operator-=, -=)
    VOTE_OPERATOR_MODIF_SCALAR(operator*=, *=)
    VOTE_OPERATOR_MODIF_SCALAR(operator/=, /=)
    VOTE_OPERATOR_SCALAR(operator*, *)
    VOTE_OPERATOR_SCALAR(operator/, /)
};

struct PackedVote
{
    Metadata metadata;
    float    weight;
    Vote     vote;
};

// Consumer functions
using ParameterConsumer  = std::function<void(CameraParameters const&)>;
using PatchConsumer      = std::function<void(Patch&&)>;
using FeatureConsumer    = std::function<void(Features&&)>;
using PackedVoteConsumer = std::function<void(PackedVote&&)>;
using FlushConsumer      = std::function<void(size_t)>;
using MultiVoteConsumer  = std::function<void(size_t, std::vector<Vote>&&)>;

} // namespace Pl

#endif // TYPES_HPP
