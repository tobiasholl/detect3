#ifndef FEATURE_EXTRACTION_HPP
#define FEATURE_EXTRACTION_HPP

#include <tensorflow/core/public/session.h>
#include <tensorflow/core/protobuf/meta_graph.pb.h>

#include "../config.hpp"
#include "../concurrency.hpp"
#include "../log.hpp"

#include "types.hpp"

#define CHECK_TF(...) if (!((__VA_ARGS__).ok())) { Log::log(Log::Error, __FILE__, ":", __LINE__, " (", __FUNCTION__, "): TensorFlow error: Status check failed."); abort(); }

namespace Pl
{

class FeatureExtractionServant
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
public:
    FeatureExtractionServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler, std::string const& checkpointPrefix)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(
              scheduler,
              Concurrent::createThreadpool(Config::FEATURE_EXTRACTION_STAGE_THREADS, "Pl::FeatureExtractionServant")
          )
        , m_options{}
    {
        // Set options
        m_options.config.set_intra_op_parallelism_threads(Config::TENSORFLOW_THREADS);
        m_options.config.set_inter_op_parallelism_threads(Config::TENSORFLOW_THREADS);
        m_options.config.mutable_gpu_options()->set_allow_growth(true);

        // Create a TensorFlow session and load the graph
        CHECK_TF(tensorflow::NewSession(m_options, &m_session));
        CHECK_TF(tensorflow::ReadBinaryProto(tensorflow::Env::Default(), checkpointPrefix + ".meta", &m_metagraph));
        CHECK_TF(m_session->Create(m_metagraph.graph_def()));

        // Load the weights from the checkpoint
        tensorflow::Tensor checkpoint(tensorflow::DT_STRING, tensorflow::TensorShape());
        checkpoint.scalar<std::string>()() = checkpointPrefix;
        CHECK_TF(m_session->Run({{ m_metagraph.saver_def().filename_tensor_name(), checkpoint },}, {}, { m_metagraph.saver_def().restore_op_name() }, nullptr));
    }

    ~FeatureExtractionServant()
    {
        m_session->Close();
    }

    PatchConsumer getPatchConsumer()
    {
        return this->make_safe_callback(
            [this](Patch&& patch)
            {
                this->eat(std::move(patch));
            },
            "Pl::FeatureExtractionServant / getPatchConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    ParameterConsumer getParameterConsumer()
    {
        return this->make_safe_callback(
            [this](CameraParameters const& params)
            {
                this->m_paramConsumer(params);
            },
            "Pl::FeatureExtractionServant / getParameterConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    FlushConsumer getFlushConsumer()
    {
        return this->make_safe_callback(
            [this](size_t id)
            {
                this->m_flush(id);
            },
            "Pl::FeatureExtractionServant / getFlushConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    template <typename T>
    void setNextStage(T const& nextStage)
    {
        m_consumer      = nextStage.getFeatureConsumer().get();
        m_paramConsumer = nextStage.getParameterConsumer().get();
        m_flush         = nextStage.getFlushConsumer().get();
    }

    void eat(Patch&& patch)
    {
        post_callback(
            [patch = std::move(patch), consumer = m_consumer, session = m_session]()
            {
                // Build input tensor
                tensorflow::Tensor input(tensorflow::DT_FLOAT, tensorflow::TensorShape({Config::PATCH_PIXEL_SIZE, Config::PATCH_PIXEL_SIZE, 4}));
                std::copy_n(patch.raw(), Config::PATCH_PIXEL_SIZE * Config::PATCH_PIXEL_SIZE * 4, input.flat<float>().data());

                // Run the feature extraction step
                std::vector<tensorflow::Tensor> outputs;
                CHECK_TF(session->Run({{ "in_layer", input },}, { "feature_layer", }, {}, &outputs));

                // Turn the result into a more usable data type
                Features features(patch.metadata);
                std::copy_n(outputs[0].flat<float>().data(), Config::FEATURE_EXTRACTION_DIMENSIONS, features.raw());

                // Pass the features onwards
                consumer(std::move(features));
            },
            [](boost::asynchronous::expected<void> const& result)
            {
                // Check that everything went well
                result.get();
            },
            "Pl::FeatureExtractionServant::eat",
            Concurrent::NORMAL_PRIORITY,
            Concurrent::LOWEST_PRIORITY
        );
    }

private:
    FeatureConsumer             m_consumer;
    ParameterConsumer           m_paramConsumer;
    FlushConsumer               m_flush;
    tensorflow::SessionOptions  m_options;
    tensorflow::MetaGraphDef    m_metagraph;
    tensorflow::Session        *m_session;
};

class FeatureExtractionProxy
    : public boost::asynchronous::servant_proxy<FeatureExtractionProxy, FeatureExtractionServant, Concurrent::Job>
{
public:
    template <typename Scheduler>
    FeatureExtractionProxy(Scheduler s, std::string const& checkpointPrefix)
        : boost::asynchronous::servant_proxy<FeatureExtractionProxy, FeatureExtractionServant, Concurrent::Job>(s, checkpointPrefix)
    {}

    BOOST_ASYNC_FUTURE_MEMBER_LOG(getPatchConsumer,     "Pl::FeatureExtractionProxy::getPatchConsumer",     Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getParameterConsumer, "Pl::FeatureExtractionProxy::getParameterConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getFlushConsumer,     "Pl::FeatureExtractionProxy::getFlushConsumer",     Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setNextStage,   "Pl::FeatureExtractionProxy::setNextStage",   Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_SERVANT_POST_CTOR_LOG("Pl::FeatureExtractionProxy()",  Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_SERVANT_POST_DTOR_LOG("Pl::~FeatureExtractionProxy()", Concurrent::NORMAL_PRIORITY)
};

} // namespace Pl

#endif // FEATURE_EXTRACTION_HPP
