#ifndef SAMPLING_HPP
#define SAMPLING_HPP

#include <cstdint>
#include <memory>
#include <tuple>

#include <boost/asynchronous/algorithm/parallel_for.hpp>
#include <boost/asynchronous/helpers/lazy_irange.hpp>

#include "../config.hpp"
#include "../concurrency.hpp"
#include "../log.hpp"

#include "types.hpp"

#include "../hardware/kinect2/framepack.hpp"

namespace Pl
{

// Mathematical constants for sampling.
constexpr double M_SQRT3_2             = sqrt(3) / 2;

// SamplingServant: Controls the sampling process (Kehl et al., p. 4). This class must be specialized for every type of input image.
//                  Images are sampled on a hexagonal grid with the given step size s to ensure that every sampling point is evenly
//                  spaced from all its neighbors. Distance between sampling lines is d = M_SQRT3_2 * s. Lines alternate between
//                  s / 2 padding on the right and s / 2 padding on the left, and should be centered in the image.
//                  Vertical margins:
//                      For l lines of sample points, we use d * (l - 1) + 1 lines of pixels. We can fit (height - 1) / d + 1 lines
//                      of samplepoints into the image.
//                                                VERTICAL SAMPLES = (height - 1) / d + 1
//                                                 VERTICAL MARGIN = (height - (d * (VERTICAL SAMPLES - 1) + 1)) / 2
//                  Horizontal margins:
//                      A line of k sample points uses s * (k - 1) + 1 pixels of space. An image can fit (width - 1) / s + 1 sample
//                      points per line. However, because we add s / 2 pixels of spacing, the width is reduced.
//                                              HORIZONTAL SAMPLES = (width - 1 - s / 2) / s + 1
//                                               HORIZONTAL MARGIN = (width - (s * (HORIZONTAL SAMPLES - 1) + 1)) / 2
template <typename Input>
class SamplingServant;

template <>
class SamplingServant<Hw::Kinect2::FramePack>
    : public boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>
{
public:
    SamplingServant(boost::asynchronous::any_weak_scheduler<Concurrent::Job> scheduler, size_t step = Config::SAMPLING_DEFAULT_STEP)
        : boost::asynchronous::trackable_servant<Concurrent::Job, Concurrent::Job>(
              scheduler,
              Concurrent::createThreadpool(Config::SAMPLING_STAGE_THREADS, "Pl::SamplingServant")
          )
        , m_imageConsumer([](Hw::Kinect2::FramePack&&) {}) // <= Avoids errors if no image consumer is set
        , m_step(step)
    {}

    Hw::Kinect2::FramePackConsumer getFramePackConsumer()
    {
        return this->make_safe_callback(
            [this](Hw::Kinect2::FramePack&& pack)
            {
                this->eat(std::move(pack));
            },
            "Pl::SamplingServant / getFramePackConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    Hw::Kinect2::ParameterConsumer getParameterConsumer()
    {
        return this->make_safe_callback(
            [this](libfreenect2::Freenect2Device::ColorCameraParams colorParameters, libfreenect2::Freenect2Device::IrCameraParams irParameters)
            {
                this->m_fx = irParameters.fx;
                this->m_fy = irParameters.fy;
                this->m_paramConsumer(CameraParameters { Config::KINECT2_IMAGE_WIDTH, Config::KINECT2_IMAGE_HEIGHT, irParameters.fx, irParameters.fy, irParameters.cx, irParameters.cy });
                this->m_registration.reset(new libfreenect2::Registration(irParameters, colorParameters));
            },
            "Pl::SamplingServant / getParameterConsumer", Concurrent::NORMAL_PRIORITY
        );
    }

    template <typename T>
    void setNextStage(T const& nextStage)
    {
        m_consumer      = nextStage.getPatchConsumer().get();
        m_paramConsumer = nextStage.getParameterConsumer().get();
        m_flush         = nextStage.getFlushConsumer().get();
    }

    template <typename T>
    void feedImagesTo(T const& stage)
    {
        m_imageConsumer = stage.getImageConsumer().get();
    }

    void setFocalLength(float fx, float fy)
    {
        m_fx = fx;
        m_fy = fy;
    }

    void eat(Hw::Kinect2::FramePack&& pack)
    {
        post_callback(
            [pack, step=m_step, consumer=m_consumer, fx=m_fx, fy=m_fy, registration=m_registration]()
            {
                // Get image dimensions
                constexpr size_t width  = Config::KINECT2_IMAGE_WIDTH;
                constexpr size_t height = Config::KINECT2_IMAGE_HEIGHT;

                // Get line step
                float lineStep = M_SQRT3_2 * step;

                // Get the margins and step counts (see description above)
                //     VERTICAL SAMPLES   = (height - 1) / d + 1
                //     HORIZONTAL SAMPLES = (width - 1 - s / 2) / s + 1
                //     VERTICAL MARGIN    = (height - (d * (V. SAMPLES - 1) + 1)) / 2
                //     HORIZONTAL MARGIN  = (width - (s * (H. SAMPLES - 1) + 1)) / 2
                size_t verticalSamples   = (size_t) ((height - 1) / lineStep + 1);
                size_t horizontalSamples = (size_t) ((width - 1 - step / 2.f) / step + 1);
                size_t verticalMargin   = (size_t) ((height - (lineStep * (verticalSamples - 1) + 1)) / 2.f);
                size_t horizontalMargin = (size_t) ((width - (step * (horizontalSamples - 1) + 1)) / 2); // No 2.f here because the entire calculation is in integers

                size_t totalSamples = verticalSamples * horizontalSamples;

                // Generate coordinate range (as a shared pointer, so it doesn't get destroyed while parallel_for is running)
                auto sampleToImage = [horizontalSamples, horizontalMargin, verticalMargin, step, lineStep](size_t const& index) -> std::tuple<size_t, size_t>
                {
                    // Translate the index into a pixel coordinate on the image.
                    size_t sampleRow = index / horizontalSamples;
                    size_t imageRow  = verticalMargin + sampleRow * lineStep;
                    size_t sampleCol = index % horizontalSamples;
                    size_t imageCol  = horizontalMargin + (sampleRow % 2 ? step / 2 : 0) + sampleCol * step;

                    return {imageRow, imageCol};
                };
                auto coordRange = std::make_shared<boost::asynchronous::detail::lazy_irange<decltype(sampleToImage), size_t>>(0, totalSamples, sampleToImage);

                // Sample the image
                return boost::asynchronous::parallel_for(
                    coordRange->begin(), coordRange->end(),
                    [pack, consumer, coordRange, fx, fy, registration](std::tuple<size_t, size_t> const& coords)
                    {
                        // Get sampling coordinates
                        auto [row, col] = coords;

                        // Get image dimensions
                        size_t width  = pack.color->width;
                        size_t height = pack.color->height;

                        // Get patch metadata
                        Metadata md;
                        md.frameID = pack.frameID;
                        md.row     = row;
                        md.col     = col;

                        // We use another Registration object here because we have all the data for it, but it isn't technically necessary
                        registration->getPointXYZ(pack.depth.get(), row, col, md.x, md.y, md.z);

                        // Get depth in millimeters
                        float depth = md.z * 1000;

                        // Skip invalid samples
                        if (depth <= 0 || std::isnan(depth) || std::isinf(depth)) return;

                        // Calculate extraction patch size in pixels
                        size_t extractionWidth  = Config::PATCH_METRIC_SIZE / depth * fx;
                        size_t extractionHeight = Config::PATCH_METRIC_SIZE / depth * fy;

                        // Skip patches that cannot be extracted in full
                        ssize_t topLeftRow = row - extractionHeight / 2;
                        ssize_t topLeftCol = col - extractionWidth / 2;
                        if (topLeftRow < 0 || topLeftRow + extractionHeight >= height || topLeftCol < 0 || topLeftCol + extractionWidth >= width) return;

                        // Compute scale necessary to reach predefined patch size
                        float scaleX = (float) extractionWidth / Config::PATCH_PIXEL_SIZE;
                        float scaleY = (float) extractionHeight / Config::PATCH_PIXEL_SIZE;

                        // Extract the patch
                        Patch patch;
                        patch.metadata = md;

                        // Get raw pointers
                        unsigned char *rawColor = (unsigned char*) pack.color->data;
                        float         *rawDepth = (float*) pack.depth->data;

                        for (size_t patchRow = 0; patchRow < Config::PATCH_PIXEL_SIZE; ++patchRow)
                        {
                            for (size_t patchCol = 0; patchCol < Config::PATCH_PIXEL_SIZE; ++patchCol)
                            {
                                // Compute offset in the unscaled patch
                                size_t offsetRow = (size_t) ((patchRow + .5f) * scaleY);
                                size_t offsetCol = (size_t) ((patchCol + .5f) * scaleX);

                                // Compute original image pixel
                                size_t originalRow = topLeftRow + offsetRow;
                                size_t originalCol = topLeftCol + offsetCol;

                                // Scale colors from [0; 255] to [-1; 1]
                                unsigned char *colorPointer = rawColor + (originalRow * width + originalCol) * 4; // Skip 4 bytes per pixel
                                patch.b[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = colorPointer[0] / 127.5f - 1.f;
                                patch.g[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = colorPointer[1] / 127.5f - 1.f;
                                patch.r[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = colorPointer[2] / 127.5f - 1.f;

                                // De-mean and scale depth (from [z-m; z+m] via [-m; m] to [-1; 1]) and clamp
                                patch.d[patchRow * Config::PATCH_PIXEL_SIZE + patchCol] = std::clamp((rawDepth[originalRow * width + originalCol] - depth) / Config::PATCH_METRIC_SIZE, -1.f, 1.f);
                            }
                        }

                        // Pass patch forward
                        consumer(std::move(patch));
                    },
                    Config::SAMPLING_PARALLEL_FOR_CUTOFF,
                    "Pl::SamplingServant::eat / parallel_for", Concurrent::NORMAL_PRIORITY
                );
            },
            [pack, flush=m_flush, imageConsumer=m_imageConsumer](auto result)
            {
                // Verify that everything worked and send a flush command
                result.get();
                flush(pack.frameID);


                Log::log(Log::Debug, "Finished sampling for frame ", pack.frameID);
            },
            "Pl::SamplingServant::eat", Concurrent::NORMAL_PRIORITY, Concurrent::NORMAL_PRIORITY
        );
    }

private:
    PatchConsumer                  m_consumer;
    ParameterConsumer              m_paramConsumer;
    Hw::Kinect2::FramePackConsumer m_imageConsumer;
    FlushConsumer                  m_flush;
    size_t                         m_step;
    float                          m_fx;
    float                          m_fy;

    std::shared_ptr<libfreenect2::Registration> m_registration;
};

template <typename Input>
class SamplingProxy
    : public boost::asynchronous::servant_proxy<SamplingProxy<Input>, SamplingServant<Input>, Concurrent::Job>
{
public:
    template <typename Scheduler>
    SamplingProxy(Scheduler s, size_t step = Config::SAMPLING_DEFAULT_STEP)
        : boost::asynchronous::servant_proxy<SamplingProxy<Input>, SamplingServant<Input>, Concurrent::Job>(s, step)
    {}

    // C++ standard (§ 14.6.2/3) workaround
    using base_type = boost::asynchronous::servant_proxy<SamplingProxy<Input>, SamplingServant<Input>, Concurrent::Job>;
    using callable_type = typename base_type::callable_type;

    BOOST_ASYNC_FUTURE_MEMBER_LOG(getFramePackConsumer, "Pl::SamplingProxy::getFramePackConsumer", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_FUTURE_MEMBER_LOG(getParameterConsumer, "Pl::SamplingProxy::getParameterConsumer", Concurrent::NORMAL_PRIORITY)

    BOOST_ASYNC_POST_MEMBER_LOG(setNextStage, "Pl::SamplingProxy::setNextStage", Concurrent::NORMAL_PRIORITY)
    BOOST_ASYNC_POST_MEMBER_LOG(feedImagesTo, "Pl::SamplingProxy::feedImagesTo", Concurrent::NORMAL_PRIORITY)
};

} // namespace Pl

#endif // SAMPLING_HPP
