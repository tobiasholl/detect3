#include <future>
#include <getopt.h>

#include "log.hpp"
#include "hardware/kinect2/acquisition.hpp"
#include "hardware/kinect2/registration.hpp"
#include "pipeline/sampling.hpp"
#include "pipeline/featureextraction.hpp"
#include "pipeline/matching.hpp"
#include "pipeline/votefiltering.hpp"
#include "visual/visualization.hpp"

int main(int argc, char *argv[])
{
    // Parse command line arguments
    constexpr static char USAGE[] =
        "Usage:\n"
        "  detect3 [-h] [-v ...] CHECKPOINT CODEBOOK\n"
        "\n"
        "Options:\n"
        "  -h, --help         Show this help message\n"
        "  -v, --verbose      Increase logging verbosity (can be specified multiple times)\n"
        "\n"
        "Arguments:\n"
        "  CHECKPOINT         Any trained feature extraction checkpoint\n"
        "  CODEBOOK           Location of the pre-generated object codebook\n"
    ;

    constexpr static option OPTIONS[] = {
        { "help",     no_argument,       nullptr, 'h' },
        { "verbose",  no_argument,       nullptr, 'v' },
    };
    int  option_index;
    char option_code;

    // Storage for argument settings
    std::string checkpointPrefix = "";
    std::string codebookPrefix   = "";
    int verbosityModifier = 0;

    // Handle options
    opterr = 0; // Disable getopt's error messages
    while ((option_code = getopt_long(argc, argv, "hv", OPTIONS, &option_index)) != -1)
    {
        switch (option_code)
        {
            case 'h':
                // Print help and exit
                std::cout << USAGE << std::endl;
                return 0;
            case 'v':
                ++verbosityModifier;
                break;
            case '?':
                // Invalid option
                if (optopt)
                    Log::log(Log::Error, "Invalid option: -", (char) optopt);
                else
                    Log::log(Log::Error, "Invalid option: ", argv[optind - 1]);
                Log::log(Log::Error, "Try 'detect3 --help' for more information.");
                return 1;
            default:
                Log::log(Log::Error, "Invalid option code: ", option_code);
                Log::log(Log::Error, "Try 'detect3 --help' for more information.");
                return 1;
        }
    }

    // Handle positional arguments
    if (optind != argc - 2)
    {
        Log::log(Log::Error, argc - optind, " positional argument", (argc - optind == 1) ? " was" : "s were", " specified, but detect3 takes two.");
        Log::log(Log::Error, "Try 'detect3 --help' for more information.");
        return 1;
    }

    checkpointPrefix = argv[optind++];
    codebookPrefix = argv[optind++];

    // Set log level from verbosity
    Log::setLevel((Log::LogLevel) (LOG_DEFAULT_LEVEL + verbosityModifier));

    // Promise for shutdown
    std::shared_ptr<std::promise<int>> globalPromise(new std::promise<int>);
    auto globalFuture = globalPromise->get_future();

    // Create top-level schedulers
    auto hwScheduler = Concurrent::createSingleThreadScheduler("HwScheduler");
    auto plScheduler = Concurrent::createSingleThreadScheduler("PlScheduler");
    auto viScheduler = Concurrent::createSingleThreadScheduler("ViScheduler");

    // Create proxies and their servants
    Hw::Kinect2::AcquisitionProxy             acquisition(hwScheduler);
    Hw::Kinect2::RegistrationProxy            registration(hwScheduler);
    Pl::SamplingProxy<Hw::Kinect2::FramePack> sampling(plScheduler);
    Pl::FeatureExtractionProxy                featureExtraction(plScheduler, checkpointPrefix);
    Pl::MatchingProxy                         matching(plScheduler, codebookPrefix);
    Pl::VoteFilteringProxy                    voteFiltering(plScheduler);
    Vi::GUIProxy<Hw::Kinect2::FramePack>      gui(viScheduler);

    // Build pipeline
    acquisition      .setNextStage(registration);
    registration     .setNextStage(sampling);
    sampling         .setNextStage(featureExtraction);
    featureExtraction.setNextStage(matching);
    matching         .setNextStage(voteFiltering);
    voteFiltering    .setNextStage(gui);

    // Set "shortcuts"
    sampling.feedImagesTo(gui);

    // Start the cameras
    acquisition.startDefaultDevice();

    //TODO: Proper shutdown on SIGINT.
    return globalFuture.get();
}
